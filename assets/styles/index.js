

export default function (screen){
    if(screen == "/") return {color_primary:"#610B0B", text_color_primary: "#fff",text_color_dark: "#000"}
    if(screen == "/jogos") return {color_primary:"#0B3B24", text_color_primary: "#fff",text_color_dark: "#000"}
    if(screen == "/apps") return {color_primary:"#08088A", text_color_primary: "#fff",text_color_dark: "#000"}
    if(screen == "/filmes") return {color_primary:"#868A08", text_color_primary: "#000",text_color_dark: "#000"}
    if(screen == "/livros") return {color_primary:"#FF8000", text_color_primary: "#fff",text_color_dark: "#000"}
    if(screen == "/perfil") return {color_primary:"#000", text_color_primary: "#fff",text_color_dark: "#000"}
    if(screen == "/teste") return {color_primary:"#000333", text_color_primary: "#fff",text_color_dark: "#000"}
}




