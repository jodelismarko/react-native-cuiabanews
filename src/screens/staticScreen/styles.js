import { StyleSheet } from "react-native";

export default StyleSheet.create({
    common: {
        flex: 1,
        justifyContent: "space-between",
    },
    startScreen: {
        justifyContent: "center"
    },
    startScreenBG: {
        flex: 1,
        flexWrap: 'wrap',
        justifyContent: "space-between",
    }
});