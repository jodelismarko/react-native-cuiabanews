import { View, ImageBackground, Dimensions, Text, TouchableOpacity } from 'react-native';
import styles from './styles';
import React, { useState, useEffect } from 'react';
import MediaStorage from '../../core/storage/mediaStorage';
import { usePopup } from '../../core/contexts';
import { getAllStyles } from "../../core/helpers";
import { renderAppComponents } from '../../core/components/appComponent';

export default function ({ navigation, props: { components, bgImage }, styleName, footer }) {
	const [source, setSource] = useState();
	const [touches, setTouches] = useState({
		tl: false,
		tr: false,
		br: false,
		bl: false
	});
	const [timer, setTimer] = useState();

	const { width, height } = Dimensions.get("screen");

	const { showPopup, hidePopup } = usePopup();

	async function getImage() {
		try {
			if (!bgImage) return;

			let image = await MediaStorage.getFile(bgImage);

			if (image && image.substring(0, 20).indexOf('svg') > -1) {
				console.log(url, 'SVGs not supported in cacheImage');
			} else {
				setSource({ uri: image });
			}
		} catch (error) {
			console.log(error);
		} finally {
			setTimeout(() => navigation.setLoading(false, true));
		}
	}

	function onScreenTouch(e) {
		clearTimeout(timer);

		const { pageX, pageY } = e.nativeEvent;

		if (pageX < 100 && pageY < 100) {
			setTouches({ ...touches, tl: true });
		} else if (pageX > (width - 100) && pageY < 100) {
			setTouches({ ...touches, tr: true });
		} else if (pageX < 100 && pageY > (height - 100)) {
			setTouches({ ...touches, bl: true });
		} else if (pageX > (width - 100) && pageY > (height - 100)) {
			setTouches({ ...touches, br: true });
		} else {
			return;
		}

		setTimer(setTimeout(() => setTouches({ bl: false, br: false, tl: false, tr: false }), 10000));
	}

	useEffect(() => {
		if (touches.bl && touches.br && touches.tl && touches.tr) {
			showPopup(
				<View style={{ backgroundColor: "white", width: "80%", height: "80%", alignItems: "stretch" }}>
					<TouchableOpacity onPress={hidePopup} style={{ position: "absolute", top: 0, right: 0, padding: 10 }}>
						<Text style={{ color: "#fff" }}>close (x)</Text>
					</TouchableOpacity>
				</View>
			);

			setTouches({ bl: false, br: false, tl: false, tr: false });
		}

		return () => {
			clearTimeout(timer)
		}
	}, [touches]);

	useEffect(() => {
		// setTimeout(() => navigation.setLoading(false));

		return () => navigation.setLoading(true);
	}, [navigation.currentScreen]);

	useEffect(() => {
		getImage();
	}, [bgImage]);

	return (
		<View style={[styles.common, getAllStyles(styles, styleName)]} onTouchEnd={onScreenTouch}>
			{
				bgImage
					? source && (
						<ImageBackground
							source={source}
							style={getAllStyles(styles, styleName, 'BG')}>
							{renderAppComponents({ navigation, components })}
						</ImageBackground>
					)
					: renderAppComponents({ navigation, components })
			}
			{footer}
		</View>
	);
}
