import { StyleSheet } from "react-native";
import ThemeOney from '../../../assets/styles';

export default StyleSheet.create({
    common: {
        flex: 1,
    },
    privateArea: {
        backgroundColor: ThemeOney.backgroundLightGrey,
        paddingTop: 30
    }
});
