import React, { useEffect, createContext, useState, useRef } from 'react';
import { View, ScrollView } from 'react-native';
import styles from "./styles";
import BottomToTop from "../../components/bottomToTop";
import { renderAppComponents } from "../../core/components/appComponent";
import { useEventsHandler } from '../../core/helpers';

export const ScrollContext = createContext({});

export default function ({ navigation, props: { components }, styleName, footer, sidebar }) {
    const _currentY = useRef(null);
    const _contentHeight = useRef(null);
    const _scroller = useRef(null);
    const { subscribe, unsubscribe, fireEventCallbacks } = useEventsHandler();

    const [btnVisible, setBtnVisible] = useState(false);

    useEffect(() => {
        scrollToTop()
        navigation.toTop(false)
    }, [navigation.top]);

    useEffect(() => {
        setTimeout(() => {
            scrollToTop();
            navigation.setLoading(false, true);
        });

        return () => navigation.setLoading(true);
    }, [navigation.currentScreen]);

    function scrollTo(x, y) {
        if (_scroller.current)
            _scroller.current.scrollTo({ x, y, animated: true })
    }

    function scrollToTop() {
        if (_scroller.current)
            _scroller.current.scrollTo({ x: 0, y: 0, animated: true });
    }

    return (
        <ScrollContext.Provider value={{ scrollTo, scrollToTop, subscribe, unsubscribe }}>
            <View style={[styles.common, styles[styleName]]}>

                <ScrollView ref={_scroller}
                    onContentSizeChange={(w, h) => {
                        _contentHeight.current = h;
                        fireEventCallbacks("contentSizeChange", w, h);
                    }}
                    onScroll={e => {
                        _currentY.current = e.nativeEvent.contentOffset.y;
                        setBtnVisible(e.nativeEvent.contentOffset.y > 200);
                        fireEventCallbacks("scroll", e);
                    }}
                    // scrollEventThrottle={16}
                    contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }}>

                    <View style={{ justifyContent: "flex-start", flex: 1, flexDirection: sidebar ? "row" : "column" }}>
                        {sidebar}
                        <View style={{ flex: 1 }}>
                            {renderAppComponents({ navigation, components })}
                        </View>
                    </View>
                    <View style={{ justifyContent: "flex-end" }}>
                        {footer}
                    </View>
                </ScrollView>
                {btnVisible && <BottomToTop onClick={scrollToTop} />}
            </View>
        </ScrollContext.Provider >
    );
}

