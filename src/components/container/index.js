import React, { useState, useEffect, useRef } from "react";
import { View, Platform } from "react-native";
import Thema from "./styles"
import { renderAppComponents } from "../../core/components/appComponent";

export default function ({navigation, styleName, props: { components }, parentProps}) {


    return (
        <View style={Thema(Platform)[styleName]}>
            {renderAppComponents({ components, navigation, parentProps })}
        </View>
    );
}
