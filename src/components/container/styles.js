import React, { useEffect } from "react";
import { StyleSheet } from "react-native";
import Theme from "../../../assets/styles";
import { useNavigationContext } from "../../core/contexts";


export default function(Platform){
  const {currentScreen} = useNavigationContext();
  const color = Theme(currentScreen.name)
  var sizeWidth = "99%"
  var sizeHeight = 0


  if (Platform.OS === 'web'){
    sizeWidth = "24%";
    sizeHeight = 230
  }
  
  const styles = StyleSheet.create({  
    flexRowWrap:{
      flexDirection:"row",
      justifyContent:"center",
      alignItems:"center",
      flexWrap:'wrap',      
    },
    header: {
      height: 50,
      backgroundColor: color.color_primary,
      justifyContent: "center",
    },
    flexRowSpaceBetween: {
      flexDirection: "row",
      justifyContent: "space-evenly",
    },
    cards: {
      width:`${sizeWidth}`,
      minHeight: sizeHeight,
      minWidth:250,
      borderWidth:2,
      borderColor: color.color_primary,
      marginVertical: 5,
      marginHorizontal:2,
      alignSelf: "center",
      borderRadius: 10,
    },
    perfil:{
      alignSelf:"center",
      paddingTop:80,
      paddingBottom:30,
    }
  });
  return styles
}



