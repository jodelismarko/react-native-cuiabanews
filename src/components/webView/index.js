import React, { useState } from "react";
import styles from "./styles";
import WebView from "react-native-webview";
import { View, TouchableHighlight, Text } from "react-native";
import { useNavigationContext } from "../../core/contexts";

export function useWebView() {
    const { popup } = useNavigationContext();

    return {
        opeWebViewPopup: ({ uri, html, style, styleName, popupStyle, pdf = false }) => {
            popup.show(
                <View style={{ width: "90%", height: "90%", backgroundColor: "white" }}>
                    <View style={{ width: "100%", alignItems: "flex-end", padding: 10 }}>
                        <TouchableHighlight onPress={popup.hide} underlayColor="lightgray">
                            <Text style={{ fontWeight: "bold" }}>close (x)</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={{ flex: 1 }}>
                        <WV props={{
                            uri: pdf
                                ? uri
                                : uri,
                            html: html,
                            style
                        }} styleName={styleName} />
                    </View>
                </View>,
                { ...{ backgroundColor: "#000000CC" }, ...popupStyle });
        }
    };
}

export default function WV({ props: { uri, html, style }, styleName }) {
    const [webLoading, setWebLoading] = useState(false);

    return (
        <>
            <WebView
                scrollEnabled={true}
                source={{ html, uri }}
                style={[styles[styleName], style]}
                originWhitelist={["*"]}
                onFileDownload={e => {
                    console.log("TODO: Handle file download");
                }}
                onHttpError={e => {
                    console.log("TODO: Handle HTTP error");
                }}
                onError={e => {
                    console.log("TODO: Handle WebView error");
                }}
                allowsInlineMediaPlayback={true}
                mediaPlaybackRequiresUserAction={false}
                onLoadStart={() => setWebLoading(true)}
                onLoadEnd={() => setWebLoading(false)} />

        </>
    );
}
