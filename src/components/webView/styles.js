
/**
 * Auto generated component for OneySelfCare
 * Author: joao.liliu
 * Date: 4/6/2020
 */

import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1
    },
    title: {
        fontSize: 30
    },
    pdfContractViewer: {
        width: "100%",
        height: 900,
    }
});
