import React from "react";
import { View, FlatList } from "react-native";
import styles from "./styles";
import AppComponent from "../../core/components/appComponent";

const Defaults = {
    lang:"pt-PT",
    sessionTimeout: 10*60,
    contentUpdateInterval: 5,
}

//IMPORTANT: Items should be an array of LinkButton or similar
export default ({ navigation, props: { components }, styleName }) => {

    function checkIsActive(item) {
        const href = item.props.href && item.props.href.replace(`/${Defaults.lang}`, "");
        return href === navigation.currentScreen.name;
    }

    return (
        <FlatList
            horizontal={true}
            style={styles[styleName]}
            data={components}
            renderItem={({ item }) => (
                <AppComponent
                    navigation={navigation}
                    config={item}
                    parentProps={{ active: checkIsActive(item) }} />
            )}
            keyExtractor={(item, index) => index.toString()}
        />
    );
};