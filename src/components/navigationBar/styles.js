import { StyleSheet } from "react-native";

export default StyleSheet.create({
    common: {
        flex: 1
    },
    headerNavigationBar: {
        flex: 1,
        flexDirection: "row",
    },
    subMenuNavigationBar:{
        backgroundColor:"#FFF",
        position:"absolute",
        width:"100%",
        top:110
    }
});
