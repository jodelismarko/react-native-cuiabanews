
import React, { useState, useEffect } from "react";
import WebView from 'react-native-webview';
import { View } from "react-native";
import styles from "./styles";

export default function ({ navigation, props: { video }, styleName }) {
    const [videoURL, setVideoURL] = useState(null)

    useEffect(() => {
        setVideoURL(`<iframe width="100%" height="100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen src="https://www.youtube.com/embed/${video.slice(-11)}"></iframe>`)
    }, [])

    return (
        <View style={{ height: 220, width: '100%'}}>
            <WebView
                style={{ borderRadius:7}}
                source={{ html: videoURL }}
            />
        </View>
    );
}
