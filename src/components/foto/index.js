import React, { useState, useEffect } from "react";
import { Image, View, Text, Button } from "react-native";
import styles from "./styles";

export default function ({ navigation, props: { url }, styleName }) {

    return (
        <View style={{flexDirection:'column-reverse'}}>
            <Image
                source={{ uri: url }}
                style={{height:220, width:"100%", borderRadius:7}}
            />
        </View>
    );
}
