
/**
 * Auto generated component for undefined
 * Author: jodelismarko
 * Date: 5/9/2020
 */

import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1
    },
    title: {
        fontSize: 30
    }
});
