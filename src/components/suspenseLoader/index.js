import React from "react";
import { useNavigationContext } from "../../core/contexts";
import Theme from "../../../assets/styles";
import { ActivityIndicator } from "react-native";

export default function ({ ...props }){
    const {currentScreen} = useNavigationContext();
    if(currentScreen){
        var color = Theme(currentScreen.name)
    }else{
        var color = Theme("/")
    }

    return <ActivityIndicator color={color.color_primary} {...props} />
}
