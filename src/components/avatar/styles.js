import { StyleSheet } from 'react-native';
import ThemeOney from '../../../assets/styles';

export default StyleSheet.create({
    avatarContainer: {
        width: 46,
        height: 46,
        borderRadius: 27,
        borderWidth: 2,
        borderColor: ThemeOney.border_color_primary,
    },
    image: {
        flex: 1,
    },
    name: {
        fontSize: 11,
        color: ThemeOney.text_color_light,
        alignSelf: "center"
    },
    avatarMenu: {
        paddingHorizontal: 30,
        paddingVertical: 15,
        backgroundColor: ThemeOney.backgroundLight,
        position: "absolute",
        minWidth: 200,
        top: 116,
        right: 45,
        zIndex: 100
    },
    menuItem: {
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderColor: ThemeOney.border_color_lightgrey3,

    },
    menuText: {
        fontSize: 16,
        color: ThemeOney.text_color_primary
    }
});