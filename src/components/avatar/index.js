import React from "react";
import { Text, View, Image } from "react-native";
import styles from "./styles";
import CachedImage from "../cachedImage";
import LinkButton from "../linkButton";
import ThemeOney from "../../../assets/styles";
import { useUserContext, useContextMenu } from "../../core/contexts";

export default function ({ navigation,
    props: { genderFieldName, imageMale, imageFemale, imageFieldName, nameFieldName, menu, maleGenderCode },
    styleName }) {

    const { showContextMenu } = useContextMenu();
    const { authenticated, getUserValue } = useUserContext();

    function showMenu() {
        showContextMenu(renderItems(), { top: 116, right: 45 }, {
            paddingHorizontal: 30,
            paddingVertical: 15,
            backgroundColor: ThemeOney.backgroundLight,
            minWidth: 200,
        });
    }

    function renderItems() {

        const mapItems = menu && menu.map((item, index) => (
            <LinkButton key={index}
                props={{ title: item.text, href: item.href, linkType: item.linkType }}
                styleName={item.styleName} />
        ))
        return mapItems;
    }

    function renderImage() {
        const userImage = getUserValue(imageFieldName);

        if (userImage)
            return <Image source={{ uri: userImage }} style={styles.image} />;

        const genderImageUrl = (getUserValue(genderFieldName) == maleGenderCode ? imageMale : imageFemale);
        return <CachedImage props={{ url: genderImageUrl }} style={styles.image} />
    }

    return authenticated ? (
        <View style={{ backgroundColor: "transparent" }} >
            <View style={styles.avatarContainer}>
                {renderImage()}
            </View>
            <Text style={styles.name}>
                {getUserValue(nameFieldName)}
            </Text>
        </View>
    ) : null;
}