import React, { useState, useEffect, useRef } from 'react';
import { Image } from 'react-native';
import styles from './style';
import MediaStorage from '../../core/storage/mediaStorage';

export default ({ props: { url }, styleName, style, bg, ...rest }) => {
	const [source, setSource] = useState();
	const isMounted = useRef(true);

	async function getImage() {
		if (!url) {
			return null;
		}

		const image = await MediaStorage.getFile(url);

		if (!isMounted.current) return;

		if (image && image.substring(0, 20).indexOf('svg') > -1) {
			console.log(url, 'SVGs not supported in cacheImage');
		} else if (image) {
			setSource({ uri: image });
		}
	}

	const getStyles = () => [styles.common, style, styles[styleName]];

	useEffect(() => {
		getImage();
	}, [url]);

	useEffect(() => {
		return () => isMounted.current = false;
	}, []);

	return (
		<Image source={source} style={getStyles()} {...rest} />

	);
};
