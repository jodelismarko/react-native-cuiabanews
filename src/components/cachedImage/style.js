import { StyleSheet } from 'react-native';
import ThemeOney from '../../../assets/styles';

export default StyleSheet.create({
	common: {
		resizeMode: 'contain'
	},
	perfil:{
		width: 250,
		height: 250,
		borderRadius:125,

	},
	startScreenLogo: {
		width: 150,
		height: 40,
		margin: 30,
		alignSelf: 'center'
	},
	headerLogo: {
		width: 120,
		height: 40,
		marginRight: 40,
		marginLeft: 25
	},
	privateHeaderLogo: {
		width: 120,
		height: 40,
		marginRight: 40,
		marginLeft: 25
	},
	headerBottomImage: {
		width: 100,
		height: 30
	},
	fullWidth485: {
		width:'100%',
		height:240
	},
	cards:{
		width: 150,
		height: 150,
	}
});
