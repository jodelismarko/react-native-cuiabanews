import React from 'react';
import {Text} from 'react-native';
import { ContainerStyle, TextStyle } from './styles';
import { View } from 'react-native';

export default ({ props: { text }, styleName }) => {
	return (
		<View style={ContainerStyle[styleName]}>
			<Text text={text} styleName={styleName} style={[TextStyle.common, TextStyle[styleName]]} />
		</View>
	);
};
