import { StyleSheet } from 'react-native';
import ThemeOney from '../../../assets/styles';

export const ContainerStyle = StyleSheet.create({
	formTextSubtitle: {
		borderBottomWidth: 1,
		borderColor: ThemeOney.border_color_primary,
		flex: 1,
		marginBottom: 25,
	},
	loginText: {
		marginBottom: 30
	},
	firstLoginTitle: {
		paddingTop: 15,
		paddingBottom: 20,
		textAlign: "center",
		width: 616
	},
	sendEmailSuccess:{
		backgroundColor:"#cde499",
		marginTop:20,
		width:685,
		padding:20
	}
});

export const TextStyle = StyleSheet.create({
	common: {
		fontFamily: 'Arial',
	},
	formTextTitle: {
		fontSize: 26,
		color: ThemeOney.text_color_green,
		paddingVertical: 40,
	},
	formTextTitleDark: {
		fontSize: 26,
		color: ThemeOney.text_color_darkgrey3,
		paddingVertical: 40,
	},
	formTextSubtitle: {
		fontSize: 22,
		color: ThemeOney.text_color_darkgrey3,
		paddingBottom: 20,
	},
	formTextDisclaimer: {
		fontSize: 18,
		color: ThemeOney.text_color_primary,
		paddingBottom: 60,
	},
	footerBottomBar: {
		color: ThemeOney.text_color_primary,
		fontSize: ThemeOney.font_size_bottombar_footer,
		lineHeight: 23,
	},
	loginTitle: {
		color: ThemeOney.text_color_login_title,
		fontSize: 22,
	},
	loginText: {
		fontSize: 18,
		paddingTop: 30
	},
	insuranceText: {
		marginTop: 10,
		fontSize: 18,
		paddingBottom: 5,
		color: ThemeOney.text_color_primary
	},
	formTitleOtp: {
		fontSize: 26,
		color: ThemeOney.text_color_green,
	},
	productCardTitle: {
		color: ThemeOney.text_color_primary,
		fontSize: 16,
		marginLeft: 15,
		marginBottom: 10,
	},
	productCardTitleBold: {
		color: ThemeOney.text_color_primary,
		fontWeight: "bold",
		fontSize: 16,
		marginLeft: 15,
		marginBottom: 10,
	},
	infoDataProcessing: {
		fontSize: 18,
		color: ThemeOney.text_color_primary,
	},
	firstLoginTitle: {
		color: "#81BC00",
		fontSize: 30,
		fontWeight: "bold",
	},
	paymentMethodTitle: {
		fontWeight: "bold",
		fontSize: 16,
		paddingHorizontal: 0,
		color: ThemeOney.text_color_primary,
	},
	sendEmailSuccess: {
		color: "#6F6F6F",
		fontSize: 16,
		fontWeight: "bold",
	},
	titlesPrivateArea: {
		fontSize: 26,
		fontFamily: 'Arial',
		fontWeight:	 "bold",
		color: ThemeOney.text_color_darkgrey3,
	}
});
