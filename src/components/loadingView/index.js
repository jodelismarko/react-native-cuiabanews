import React from "react";
import Thema from "./styles"
import {Text, View} from "react-native";
import SuspenseLoader from "../suspenseLoader";

export default function ({ title, subtitle, viewStyle, ...rest }) {
    return (
        <View style={[Thema().container, viewStyle]} {...rest}>
            <SuspenseLoader size="large" />

            {title && (
                <View>
                    <Text style={Thema().title}>{title}</Text>
                </View>
            )}

            {subtitle && (
                <View>
                    <Text style={Thema().subtitle}>{subtitle}</Text>
                </View>
            )}
        </View>
    );
}

