import { StyleSheet } from "react-native";
import Theme from "../../../assets/styles";
import { useNavigationContext } from "../../core/contexts";

export default function() {
  const { currentScreen } = useNavigationContext();

  if (currentScreen) {
    var color = Theme(currentScreen.name);
  } else {
    var color = Theme("/");
  }

  const styles = StyleSheet.create({
    container: {
      position: "absolute",
      left: 0,
      top: 0,
      right: 0,
      bottom: 0,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "white",
    },
    title: {
      padding: 30,
      textAlign: "center",
      color: color.text_color_primary,
      fontSize: 12,
    },
    subtitle: {
      textAlign: "center",
      fontSize: 12,
      color: color.text_color_primary,
    },
  });
  return styles;
}
