import React from "react";
import {BtnImage, BoxImage} from './styles';
import { Image, TouchableOpacity } from "react-native";


export default function ({ onClick }) {    
    
    return (
        <TouchableOpacity onPress={onClick} style={BoxImage().common}>
            <Image source={require("../../../assets/images/arrow_top.png")} style={BtnImage.common} />
        </TouchableOpacity>
    );
}