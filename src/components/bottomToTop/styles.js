import { StyleSheet } from "react-native";
import Theme from "../../../assets/styles";
import { useNavigationContext } from "../../core/contexts";

export const BtnImage =  StyleSheet.create({
    common: {
        width: 20,
        height: 10,
    }
});

export function BoxImage(){
  const {currentScreen} = useNavigationContext();
  
  const color = Theme(currentScreen.name)

  const BoxImage = StyleSheet.create({
    common: {
        width: 50,
        height: 50,
        bottom: 5,
        right: 5,
        borderRadius: 34,
        position: 'absolute',
        backgroundColor: color.color_primary,
        justifyContent: "center",
        alignItems: "center"
    }
});
  return BoxImage
}



