import { StyleSheet } from 'react-native';
import ThemeOney from '../../../assets/styles';
import Theme from "../../../assets/styles";

function btnContainer(screen){
	const color = Theme(screen)
  
	const BtnContainer = StyleSheet.create({
		common: {
			borderBottomColor: "#FF3",
			borderBottomWidth:2
		}
	});
	return BtnContainer
  }

export const BtnContainerSubMenu = StyleSheet.create({
	subMenuActive: {
		borderColor: ThemeOney.border_color_primary,
		paddingBottom: 10,
		borderBottomWidth: 4
	},
	subMenuStartActive: {
		borderColor: ThemeOney.border_color_primary,
		paddingBottom: 10,
		borderBottomWidth: 4
	}
});

export const BtnUnderlay = {
	primary: {
		borderColor: ThemeOney.button_bg_color_primary,
		backgroundColor: ThemeOney.button_bg_color_primary
	},
	default: {
		borderColor: ThemeOney.button_bg_color_primary,
		backgroundColor: ThemeOney.button_bg_color_primary
	}
};
export const BtnImage = StyleSheet.create({
	common: {
		width: 30,
		height: 30,
		backgroundColor: 'transparent',
	},
	navigationLink: {
		height: 30,
		width: 30,
	},
	navigationLinkAvatar: {
		borderColor:'#fff',
		borderWidth:2,
		borderRadius:15,
		height: 30,
		width: 30,
	},
	navigationLinkActive: {
		height: 20,
		width: 20,
	}
});

function loadcColors(screen){
	const color = Theme(screen)
  
	const BtnText = StyleSheet.create({
		common: {
		},
		default: {
			fontSize: 22,
			color: color.text_color_primary
		},
		navigationLink: {
			fontSize: 12,
			color: color.text_color_primary
		},
		navigationLinkAvatar: {
			fontSize: 12,
			color: color.text_color_primary
		}
	});
	return BtnText
  }
  
  export function BtnContainer(screen){
	return btnContainer(screen)
  }
  
  export function BtnText(screen){
	return loadcColors(screen)
  }
