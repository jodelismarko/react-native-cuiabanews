import React, { useState, useEffect } from "react";
import { Text, Image, View, TouchableWithoutFeedback } from "react-native";
import { BtnContainer, BtnImage, BtnText, BtnUnderlay, BtnContainerSubMenu } from "./styles";
import { useNavigationContext, useUserContext } from "../../core/contexts";
import { useOnLinkPressHandler } from "../../core/helpers";

export default function ({ navigation, props: { title, href, linkType, image, showIf, checkIsActive }, styleName, parentProps }) {
    const { onLinkPress } = useOnLinkPressHandler({ href, linkType });
    const [touching, setTouching] = useState(false);
    const [visible, setVisible] = useState(true);
    const [active, setActive] = useState(false);
    const { user } = useUserContext();
    const { pageData, currentScreen, actions } = useNavigationContext();

    if (showIf) {
        useEffect(() => {
            actions.execute(showIf, parentProps).then(show => {
                if (typeof (show) === "boolean") {
                    setVisible(show);
                }
            });
        }, [user, pageData, parentProps]);
    }

    function getStyles(styleGroup, styleName, suffix) {
        if (!styleName)
            return null;
        const styleNames = styleName.split(",");
        var stylesArray = styleNames.map(x => styleGroup[suffix ? x + suffix : x]);

        return stylesArray;
    }

    function getBtnUnderlayStyle() {
        const styleNames = styleName.split(",");
        return BtnUnderlay[Object.keys(BtnUnderlay).find(x => styleNames.find(y => y == x))];
    }

    useEffect(() => {
        if (checkIsActive) {
            actions.execute(checkIsActive, parentProps).then(res => {
                setActive(res);
            });
        } else {
            setActive((parentProps && parentProps.active) || false);
        }
    }, [currentScreen]);

    return visible ? (
        <TouchableWithoutFeedback onPress={onLinkPress}>

            <View style={active ? {borderColor:"#FF3", borderWidth:4}:null}>
                <View style={{ flexDirection: "column", alignItems:'center', width: 60}}>
                    {image != null && image != "" &&
                        <Image source={{ uri: image }} style={[BtnImage.common, getStyles(BtnImage, styleName)]} />
                    }
                    <Text style={[BtnText(currentScreen.name).common, getStyles(BtnText(currentScreen.name), styleName)]}>{title}</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    ) : null;
}
