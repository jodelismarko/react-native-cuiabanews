import { StyleSheet } from "react-native";
import Theme from "../../../assets/styles";
import { useNavigationContext } from "../../core/contexts";

export const common = StyleSheet.create({
	h1: {
		fontSize: 18,
		fontWeight: "bold",
		textAlign:'center',
		color: "#000",
	},
	h2: {
		fontSize: 14,
		fontWeight: "bold",
		color: "#000",
	},
	p: {
		fontSize: 12,
		textAlign:'justify',
		color: "#000",
	},
	a: {
		fontSize: 12,
		color: "#000",
	}
});


export default function(){
  const {currentScreen} = useNavigationContext();
  
  const color = Theme(currentScreen.name)
  
  const styles ={
	h1: {
		fontSize: 18,
		fontWeight: "bold",
		textAlign:'center',
		color: color.color_primary,
	},
	h2: {
		fontSize: 14,
		fontWeight: "bold",
		color: color.text_color_dark,
	},
	p: {
		fontSize: 12,
		textAlign:'justify',
		color: color.text_color_dark,
	},
	a: {
		fontSize: 12,
		color: color.text_color_dark,
	}
};
  return styles
}





