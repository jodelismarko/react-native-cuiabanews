import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';
import HtmlView from 'react-native-htmlview';
import styles, { common } from './styles';
import { JsUtils } from '../../core/utils';
import { usePopup } from '../../core/contexts';
import { useNavigationContext } from "../../core/contexts";

export default function ({ props: { abstract, htmlContent, textStyle, transformTextAction }, styleName }) {
	const [content, setContent] = useState("");
	const { actions } = useNavigationContext();
	const { showPopup, hidePopup } = usePopup();


	useEffect(() => {
		htmlContent = htmlContent ? htmlContent.replace(/\n/gm, '') : htmlContent;
		setContent(htmlContent);

		if (transformTextAction) {
			actions.execute(transformTextAction, {
				text: htmlContent,
				tokensReplacer: JsUtils.tokensReplacer
			}).then(setContent);
		}
	}, [htmlContent]);

	function handlerPopup() {
		showPopup(
			<View style={{ width: "90%", maxHeight: '90%', alignItems: "stretch", backgroundColor:'#fff' }}>
				<ScrollView >
					<HtmlView
						value={htmlContent}
						style={{paddingTop:30, paddingHorizontal:10, paddingBottom:10}}
						stylesheet={common}
						addLineBreaks={true}
						lineBreak={'\n'}
						paragraphBreak={'\n'}
					/>
				</ScrollView>
				<TouchableOpacity onPress={hidePopup} onClick={hidePopup} style={{ backgroundColor:'#fff', position: "absolute", width:'100%', padding: 5, flexDirection:'row',justifyContent:'flex-end' }}>
					<Text style={{ color: "#000" }}>close (x)</Text>
				</TouchableOpacity>
			</View>
		);
	}



	return (
		<View onTouchEnd={handlerPopup} onClick={handlerPopup} style={{paddingHorizontal:5, paddingBottom:10, alignSelf:"center"}}>
			<HtmlView
				value={abstract}
				stylesheet={styles()}
				addLineBreaks={true}
				lineBreak={'\n'}
				paragraphBreak={'\n'}
			/>
		</View>

	);
};