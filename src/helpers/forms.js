import { createContext, useContext, useEffect } from "react";
import { useFormikContext, useField } from "formik";
import { JsUtils } from "../core/utils";

export const FormGroupContext = createContext({});

export function useFormGroupContext(context) {
    const _context = context || useContext(FormGroupContext);

    return _context;
}

export function useFormikMeta(meta) {

    if (typeof meta.error === "object" && Object.keys(meta.error).length == 0) {
        meta.error = "";
    }

    return meta;
}

export function useFormikField(name) {
    name = parseFieldName(name);

    const { fieldConfigs, setPrefilled, setFieldVisibility } = useFormGroupContext();
    const [field, meta, helpers] = useField(name);
    const config = fieldConfigs[name] || { visible: true, prefilled: false };

    helpers.setValue = ((realSetValue) =>
        (value, prefilled = false) => {
            const isPrefilled = config.prefilled;
            setPrefilled(name, prefilled);
            realSetValue(value);
            if (isPrefilled) {
                setTimeout(() => helpers.setTouched(isPrefilled));
            }
        }
    )(helpers.setValue);

    return [
        field,
        {
            ...meta,
            ...{ config }
        },
        {
            ...helpers,
            ...{
                setPrefilled: (prefilled = true) => setPrefilled(name, prefilled),
                setVisible: (visible = true) => setFieldVisibility(name, visible)
            }
        }
    ];
}

export const parseFieldName = (fieldName) => fieldName.replace(".", "___");
export const getRealFieldName = (fieldName) => fieldName.replace("___", ".");
export const getFormFieldNamePath = (fieldName) => fieldName.split("___");
export const getRealFieldNamePath = (fieldName) => fieldName.split(".");

export function useRelativeDate({ fieldName, setDate, defaultValue }) {
    fieldName = parseFieldName(fieldName);
    const { values } = useFormikContext();

    useEffect(() => {
        const value = values[fieldName];
        const date = JsUtils.stringToDate(value);
        setDate(date || defaultValue);

    }, [values[fieldName]]);

    return defaultValue;
}

export function setDateLimitHandler(value, setDate, defaultValue) {
    let date = defaultValue;

    if (value) {
        const [type, fieldName] = value.split(",").map(x => x.trim());

        if (typeof (InstructionHandlers.date[type]) === "function") {
            date = InstructionHandlers.date[type]({ fieldName, setDate, defaultValue });
        }
    }

    useEffect(() => {
        setDate(date);
    }, []);
}

export const FieldValidations = {
    isNifValid(nif) {
        let c;
        let checkDigit = 0;

        if (nif != null && nif.length == 9) {
            c = nif.charAt(0);

            if (c == "1" || c == "2" || c == "3" || c == "5" || c == "6" || c == "8" || c == "9") {
                checkDigit = c * 9;

                for (i = 2; i <= 8; i++) {
                    checkDigit += nif.charAt(i - 1) * (10 - i);
                }

                checkDigit = 11 - (checkDigit % 11);

                if (checkDigit >= 10) {
                    checkDigit = 0;
                }

                if (checkDigit == nif.charAt(8)) {
                    return true;
                }
            }
        }

        return false;
    },

    isIBANValid(iban) {
        //required will validate this part
        if (iban === null || iban === "") {
            return true;
        }

        //generic IBAN validation
        iban = iban.replace(/\ /g, "").toUpperCase();
        if (iban.length != 25) {
            return false;
        }
        var patt = new RegExp("^[A-Z0-9]*$");
        if (iban === null || iban === "" || !patt.test(iban) || iban.length <= 4) {
            return false;
        }
        iban = iban.substring(4) + iban.substring(0, 4);
        var checksum = 0;
        for (var i = 0; i < iban.length; i++) {
            var c = iban[i];
            var temp;
            if (!isNaN(c)) {
                temp = Number(c);
            } else {
                temp = c.charCodeAt(0) - 55;
                checksum *= 10;
            }
            checksum *= 10;
            checksum += temp;
            checksum %= 97;
        }
        return checksum === 1;
    }
};

export const InstructionHandlers = {
    date: {
        MoreThanEighteenYearsDate() {
            const date = new Date();
            date.setFullYear(date.getFullYear() - 18);
            return date;
        },

        MinIdDocumentDate: () => new Date(),//TODO

        DateGreaterThanField: useRelativeDate,

        DateLessThanField: useRelativeDate,

        GetDatetimeNow: () => new Date(),

        now: () => new Date(),

        age(years) {
            const date = new Date();
            date.setFullYear(date.getFullYear() - years);
            return date;
        }
    },

    mixed: {
        operators: {
            is: (value, comparison) => value == comparison,

            equal: (value, comparison) => value == comparison,

            not: (value, comparison) => value != comparison,

            notequal: (value, comparison) => value != comparison,

            in: (value, comparison) => comparison instanceof Array && comparison.includes(value),

            notin: (value, comparison) => comparison instanceof Array && !(comparison.includes(value)),

            contains: (value, comparison) => (typeof value === "string" || value instanceof Array) && value.includes(comparison)
        }
    }
};
