import * as yup from 'yup';
import { InstructionHandlers, parseFieldName } from './forms';
import { JsUtils } from '../core/utils';
import { FieldValidations } from "../helpers/forms";

export default {
	//Required for validations
	setupValidation(dictionary) {

		function getValidationMessage(key, defaultValue, ...args) {
			return args.length ? JsUtils.formatString(
				dictionary["Validations/" + key] ?? defaultValue,
				args,
			) : dictionary["Validations/" + key] ?? defaultValue;
		}

		yup.addMethod(yup.string, 'nifValidator', function (message) {
			return this.test({
				name: 'is-nif-valid',
				message: message || getValidationMessage("InvalidNIF", 'Invalid NIF.'),
				test: nif => {
					return FieldValidations.isNifValid(nif);
				},
			});
		});

		yup.addMethod(yup.string, 'ibanValidator', function (message) {
			return this.test({
				name: 'is-iban-valid',
				message: message || getValidationMessage("InvalidIBAN", 'Invalid IBAN.'),
				test: iban => {
					return FieldValidations.isIBANValid(iban);
				},
			});
		});

		yup.addMethod(yup.mixed, 'requiredIf', function (
			fieldName,
			operator,
			comparisonValues,
		) {
			return this.when(fieldName, (value, schema) => {
				const checkResult = operator
					? InstructionHandlers.mixed.operators[operator](
						value,
						comparisonValues || true,
					)
					: JsUtils.toBoolean(value);

				return checkResult ? schema.required() : schema;
			});
		});

		yup.addMethod(yup.date, 'afterThan', function (fieldName) {
			return this.when(fieldName, (value, schema) => {
				return value ? schema.min(value) : schema;
			});
		});

		yup.addMethod(yup.date, 'beforeThan', function (fieldName) {
			return this.when(fieldName, (value, schema) => {
				return value ? schema.max(value) : schema;
			});
		});

		yup.addMethod(yup.date, 'before', function (option, comparisonValues, message) {
			return this.max(InstructionHandlers.date[option](comparisonValues), message);
		});

		yup.addMethod(yup.date, 'after', function (option, comparisonValues, message) {
			return this.min(InstructionHandlers.date[option](comparisonValues), message);
		});

		yup.addMethod(yup.mixed, "requiredIfGlobal", function (operator, objectName, message) {
			return this.test({
				name: "required-if-global",
				message,
				test(value) {
					let result = true;

					const comparisonValues = contextValues[objectName];
					if (comparisonValues)
						result = InstructionHandlers.mixed.operators[operator](value, comparisonValues);

					return result;
				}
			});
		});

		yup.addMethod(yup.mixed, 'validIf', function (fieldName, operator, comparisonValues, message) {
			return this.test({
				name: "valid-if",
				message,
				test(value) {
					const formFieldName = parseFieldName(fieldName);
					const v = this.parent[formFieldName];

					const checkResult = operator
						? InstructionHandlers.mixed.operators[operator](v, comparisonValues || true)
						: JsUtils.toBoolean(value);

					return checkResult;
				}
			});
		});

		yup.addMethod(yup.mixed, 'validIfField', function (fieldName, operator, message) {
			return this.test({
				name: "valid-if-field",
				message,
				test(value) {
					const formFieldName = parseFieldName(fieldName);
					const fieldValue = this.parent[formFieldName];

					const checkResult = InstructionHandlers.mixed.operators[operator](value, fieldValue);

					return checkResult;
				}
			});
		});

		var yupLocaleObject = {
			mixed: {
				required: getValidationMessage('Mixed/Required', 'Required field.'),
				default:
					getValidationMessage('Mixed/InvalidField', 'Invalid field.'),
			},
			string: {
				matches:
					getValidationMessage('String/InvalidField', 'Invalid field.'),
				max: getValidationMessage('String/Max', 'The field must have no more than ${max} characters.'),
			},
			date: {
				min: f =>
					getValidationMessage(
						'Date/Min', 'Must be lower than {0}.',
						JsUtils.dateToString(f.min)),
				max: f =>
					getValidationMessage(
						'Date/Max', 'Must be greater than {0}.',
						JsUtils.dateToString(f.max)),
			},
		};

		yup.setLocale(yupLocaleObject);
	}
};
