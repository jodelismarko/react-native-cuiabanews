import { lazy } from 'react';

export const Screens = {
    StaticScreen: lazy(() => import('../screens/staticScreen')),
    ScrollScreen: lazy(() => import('../screens/scrollScreen')),
};

export const Headers = {};

export const Footers = {};

//Add all components
export const Components = {
    Foto: lazy(() => import("../components/foto")),
    Video: lazy(() => import("../components/video")),
    TextField: lazy(() => import("../components/textField")),
    Image: lazy(() => import("../components/cachedImage")),
    LinkButton: lazy(() => import("../components/linkButton")),
    Container: lazy(() => import("../components/container")),
    NavigationBar: lazy(() => import("../components/navigationBar")),
    HtmlView: lazy(() => import("../components/htmlView")),
    BottomToTop: lazy(() => import("../components/bottomToTop")),
    Avatar: lazy(() => import("../components/avatar")),
    WebView: lazy(() => import("../components/webView")),
};
