import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
      flex:1, 
      height: 0, 
      width: '100%' 
    }
  });