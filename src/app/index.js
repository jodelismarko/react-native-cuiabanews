import React from 'react';
import styles from './styles'
import Navigation from '../core/navigation'
import SitecoreOfflineData from './data'
import { SafeAreaView } from 'react-native';
import { UserProvider } from '../core/contexts'

export default () => {

    return (
            <SafeAreaView style={styles.container}>
                <UserProvider>
                    <Navigation config={SitecoreOfflineData}></Navigation>
                </UserProvider>
            </SafeAreaView>
    );
};


