export default {
    "initialScreen": "/",
    "screens": [
      {
        "name": "/",
        "type": "ScrollScreen",
        "header": "Header",
        "props": {
          "components": [{
            "type" : "Container",
            "styleName":"flexRowWrap",
            "props":{
              "components":[
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A banana tem sementes?</h1><h2>Apesar do que muitos pensam, dependendo da espécie analisada, a banana tem, sim, sementes.</h2><p>Bananeira é o nome dado às plantas agrupadas no gênero conhecido como Musa. Essas espécies possuem folhas grandes e verdes que possuem bainhas que formam um pseudocaule. O caule verdadeiro da bananeira, também chamado de rizoma, é subterrâneo e cresce horizontalmente no solo. A bananeira produz um fruto que é denominado de banana e que é bastante apreciado por toda a população, sendo normalmente consumido in natura.</p>",
                        "htmlContent": "<h1>A banana tem sementes?</h1><h2>Apesar do que muitos pensam, dependendo da espécie analisada, a banana tem, sim, sementes.</h2><p>Bananeira é o nome dado às plantas agrupadas no gênero conhecido como Musa. Essas espécies possuem folhas grandes e verdes que possuem bainhas que formam um pseudocaule. O caule verdadeiro da bananeira, também chamado de rizoma, é subterrâneo e cresce horizontalmente no solo. A bananeira produz um fruto que é denominado de banana e que é bastante apreciado por toda a população, sendo normalmente consumido in natura.</p><p>Apesar de bastante difundido, o fruto não é bem conhecido pela população no que diz respeito, principalmente, à sua estrutura. Muitas pessoas acreditam que os pontinhos pretos que existem no centro da banana são sementes. Entretanto, aquelas estruturas são óvulos não fecundados. A banana cultivada é um fruto partenocárpico, ou seja, que se desenvolve sem que haja fecundação e, portanto, não possui semente.</p><p>O desenvolvimento de bananas sem sementes ocorreu em virtude da seleção desses frutos por produtores que queriam melhorar a qualidade do produto oferecido. Provavelmente, o desenvolvimento inicial desse tipo de fruto ocorreu em razão de alguma mudança genética.</p><p>Vale destacar que ainda hoje é possível encontrar bananas com sementes na natureza. Essas bananas selvagens normalmente desenvolvem sementes em situações de estresse ambiental como uma forma de garantir a sua sobrevivência. Um exemplo de banana que possui sementes é a Musa balbisiana, nativa do sul da Ásia.</p><p>A reprodução da bananeira normalmente é feita por via vegetativa, em que ocorre a separação de brotos da planta-mãe que darão origem a outro indivíduo. Nesse caso, a planta não possui variedade genética, sendo idêntica à planta-mãe. Isso representa um grande problema, uma vez que um espécime com facilidade de contrair doenças dará origem a outro com esse mesmo problema.</p><p>A produção também pode ser feita pelo fracionamento de rizoma, técnica que consiste na plantação de pedaços do caule da planta. Existe ainda a possibilidade de fazer a propagação do vegetal por técnicas de produção de mudas in vitro.</p><p>Curiosidade: As bananas-prata, prata-anã e pacovan são as variedades mais cultivadas no Brasil. De acordo com a Embrapa, essas três espécies são responsáveis por 60% da área cultivada com banana no país.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "Foto",
                      "styleName": "fullWidth485",
                      "props": {
                        "components": [],
                        "url": "https://splashbase.s3.amazonaws.com/splitshire/regular/768x506xSplitShire_IMG_8150-768x506.jpg.pagespeed.ic.KYptVn9mXX.jpg",
                        "href": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>5 filmes para quem ama dinossauros</h1><p>Você ama dinossauros? Então não pode deixar de assistir à franquia Jurassic Park e às animações Em Busca do Vale Encantado e O Bom Dinossauro.</p>",
                        "htmlContent": "<h1>5 filmes para quem ama dinossauros</h1><p>Você ama dinossauros? Então não pode deixar de assistir à franquia Jurassic Park e às animações Em Busca do Vale Encantado e O Bom Dinossauro.</p><p>Os dinossauros, sem dúvidas, mexem com o imaginário de jovens e adultos. Ficamos muitas vezes impressionados com o tamanho de algumas espécies, com a ferocidade de outras e ainda com a ideia de que espécies que dominaram a Terra por tanto tempo simplesmente não estão presentes nos dias atuais.</p><p>Fato é que a indústria do cinema nos ajuda com a tarefa de imaginar como era o mundo quando os dinossauros estavam presentes, mesmo que às vezes as representações contenham erros. A seguir listaremos alguns filmes especialmente selecionados para os amantes dos dinossauros.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção do purgatório</h1><p>Na Idade Média notamos o desenvolvimento de uma série de fatos e experiências históricas que fizeram da Igreja uma das mais poderosas instituições daquela época. A difusão dos preceitos cristãos pela Europa e em outras partes do mundo fez com que os dirigentes desta denominação religiosa interferissem profundamente nos hábitos, concepções e modos de agir de um grande número de pessoas daquela época.</p>",
                        "htmlContent": "<h1>A invenção do purgatório</h1><p>Na Idade Média notamos o desenvolvimento de uma série de fatos e experiências históricas que fizeram da Igreja uma das mais poderosas instituições daquela época. A difusão dos preceitos cristãos pela Europa e em outras partes do mundo fez com que os dirigentes desta denominação religiosa interferissem profundamente nos hábitos, concepções e modos de agir de um grande número de pessoas daquela época.</p><p>Apesar da visibilidade de todo esse processo, não podemos chegar à simplista conclusão de que os clérigos conseguiam fazer com que as pessoas fizessem aquilo que eles bem entendessem. A Igreja influiu na sociedade de sua época, mas também houve situações em que essa poderosa religião também teve de dialogar com as situações e impasses gerados pelos seus próprios seguidores. Para compreendermos tal ponto, podemos tomar a questão da vida após a morte como um interessante exemplo.</p><p>Até o século XII, o cristão estava destinado às glórias e o conforto dos céus ou ao tormento eterno mantido nas profundezas do inferno. A proposição de destinos tão diferentes, fez com que vários fiéis buscassem uma vida predominantemente voltada para a garantia de salvação. Mas como bem sabemos, desde aquele tempo, os pecados atingiam a muitos cristãos e, por isso, pairava uma enorme dúvida sobre qual seria o destino de alguém que não foi nem completamente bom ou ruim.</p><p>Nesse período, é interessante frisarmos que a ordenação social legitimada pela Igreja passava a escapar do seu controle. O mundo medieval antes dividido entre clero, nobreza e servos passava a ganhar a entrada de pessoas que não se ajustavam completamente a esse modelo harmônico dos clérigos medievais. Passando a viver no efervescente ambiente urbano, muitos fiéis e clérigos não tinham meios seguros para dizer se alguém levou ou não uma vida louvável aos olhos de Deus.</p><p>De fato essa discussão era bastante antiga e já tinha presença nos escritos de Santo Agostinho, no século IV. Segundo esse teólogo medieval, o indivíduo que teve uma vida mais inclinada ao pecado seria destinado ao Inferno, mas poderia sair dessa condição através das orações feitas pelos vivos em sua memória. Já aqueles que não foram inteiramente bons passariam por um estágio de purificação que poderia trazê-lo para os céus.</p><p>Até então, o purgatório era compreendido como um processo de salvação espiritual que fugia do que era normalmente convencionado pela Igreja. Segundo a pesquisa de alguns historiadores, a ideia de que o purgatório fosse um “lugar à parte” somente tomou forma entre os séculos XII e XIII. Contudo, engana-se quem acredita que esse terceiro destino no post mortem seja uma proposta originalmente concebida pela cristandade ocidental.</p><p>Os próprios judeus acreditavam que aqueles que não eram nem bons ou maus seriam levados a um lugar onde a pessoa sofreria castigos temporários até que estivessem aptos para viverem no éden. Entre os indianos, os “intermediários” poderiam viver uma série de reencarnações que os levariam até os céus ou ao inferno. Sem dúvida, podemos ver como a própria condição do homem e sua experiência histórica influíram na visão de mundo de várias crenças.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A luz do vaga-lume</h1><p>Vaga-lumes ou pirilampos são insetos das famílias Elateridae, Fengodidae ou Lampyridae muito conhecidos por sua bioluminescência, isto é, sua capacidade de produzir e emitir luz. Essas espécies são dotadas de órgãos fosforescentes na parte inferior de seus segmentos abdominais, responsáveis pelas emissões luminosas.</p>",
                        "htmlContent": "<h1>A luz do vaga-lume</h1><p>Vaga-lumes ou pirilampos são insetos das famílias Elateridae, Fengodidae ou Lampyridae muito conhecidos por sua bioluminescência, isto é, sua capacidade de produzir e emitir luz. Essas espécies são dotadas de órgãos fosforescentes na parte inferior de seus segmentos abdominais, responsáveis pelas emissões luminosas.</p> <p>A bioluminescência é causada pela transformação da energia química em energia luminosa. Esse processo, chamado de 'oxidação biológica', permite que a energia luminosa seja produzida sem que haja a produção de calor. Esse processo ocorre da seguinte maneira: uma molécula de luciferina é oxidada, formando uma molécula de oxiluciferina; quando essa molécula perde sua energia, emite a luz.</p> <p>Esses insetos possuem total controle sobre a emissão de luz, uma vez que o tecido que provoca essa emissão é ligado à traquéia e ao cérebro do vaga-lume. O inseto usa sua bioluminescência para chamar a atenção de seu parceiro ou parceira, por isso, essa habilidade é muito importante no processo de reprodução dessas espécies. Nesse sentido, a iluminação artificial das cidades, que é mais forte, anula a bioluminescência dos vaga-lumes, afetando diretamente o seu processo de reprodução.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p>",
                        "htmlContent": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p><p>Leonardo Da Vinci foi aclamado como um dos maiores expoentes dessa nova tendência. Dono de uma curiosidade voraz, esse italiano atuou em variados campos do conhecimento e da arte. Em alguns de seus manuscritos remanescentes temos a presença de propostas e invenções que falam sobre música, filosofia, botânica, escultura, pintura, urbanismo e engenharia. Atirando para todo o lado, ele acabou sendo um dos primeiros a teorizar uma solução para os problemas de visão.</p><p>Por volta de 1508, ele imaginou a construção de uma lente que, posta na superfície do globo ocular, poderia corrigir os problemas de visão. No século XVII, o filósofo, físico e matemático francês René Descartes foi autor de uma ideia semelhante. No entanto, apesar dos importantes precursores, as populares lentes de contato só foram primordialmente desenvolvidas nos fins do século XIX, pelo fabricante de peças óticas F. E. Muller e o médico suíço Adolf Eugen Fick.</p><p>Os protótipos iniciais não tiveram muito sucesso por conta da limitação tecnológica dessa época. As primeiras lentes de contato foram desenvolvidas por meio do uso do vidro e, por conta da rigidez do material, acabava ferindo os olhos de seus usuários. No ano de 1929, o oftalmologista nova-iorquino William Feinbloom desenvolveu uma nova lente por meio de um material criado através da mistura do vidro e do plástico.</p><p>Apesar do avanço no tipo de material empregado, essas lentes ainda eram bastante rígidas e tinham uma dimensão que causava bastante incômodo aos seus usuários. A partir de então, o aprimoramento do material e do tamanho das lentes permitiu a popularização dos primeiros modelos comerciais. Uma das lentes mais conhecidas dessa época foi criada pelo ótico Kevin Tuhoy, em 1948.</p><p>A película gelatinosa só foi inventada no início da década de 1970, quando a empresa Bausch & Lomb concebeu lentes de contato mais confortáveis e maleáveis. Como o processo de manutenção e higienização das lentes de contato era outra chateação para os usuários, vários cientistas começaram a desenvolver um modelo que fosse descartável. Dessa forma, o usuário poderia empregar as lentes de contato somente quando fosse participar de alguma solenidade importante ou tirar uma foto.</p><p>Mesmo concedendo algumas praticidades, as lentes de contato não conseguiram tomar o lugar dos óculos, principalmente para as pessoas que sofrem com a miopia e o astigmatismo. No Brasil, apenas um por cento das pessoas portadoras de problemas oculares fazem uso de lentes de contato.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "Video",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "video":'https://www.youtube.com/watch?v=DLX62G4lc44',
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p>",
                        "htmlContent": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p><p>Leonardo Da Vinci foi aclamado como um dos maiores expoentes dessa nova tendência. Dono de uma curiosidade voraz, esse italiano atuou em variados campos do conhecimento e da arte. Em alguns de seus manuscritos remanescentes temos a presença de propostas e invenções que falam sobre música, filosofia, botânica, escultura, pintura, urbanismo e engenharia. Atirando para todo o lado, ele acabou sendo um dos primeiros a teorizar uma solução para os problemas de visão.</p><p>Por volta de 1508, ele imaginou a construção de uma lente que, posta na superfície do globo ocular, poderia corrigir os problemas de visão. No século XVII, o filósofo, físico e matemático francês René Descartes foi autor de uma ideia semelhante. No entanto, apesar dos importantes precursores, as populares lentes de contato só foram primordialmente desenvolvidas nos fins do século XIX, pelo fabricante de peças óticas F. E. Muller e o médico suíço Adolf Eugen Fick.</p><p>Os protótipos iniciais não tiveram muito sucesso por conta da limitação tecnológica dessa época. As primeiras lentes de contato foram desenvolvidas por meio do uso do vidro e, por conta da rigidez do material, acabava ferindo os olhos de seus usuários. No ano de 1929, o oftalmologista nova-iorquino William Feinbloom desenvolveu uma nova lente por meio de um material criado através da mistura do vidro e do plástico.</p><p>Apesar do avanço no tipo de material empregado, essas lentes ainda eram bastante rígidas e tinham uma dimensão que causava bastante incômodo aos seus usuários. A partir de então, o aprimoramento do material e do tamanho das lentes permitiu a popularização dos primeiros modelos comerciais. Uma das lentes mais conhecidas dessa época foi criada pelo ótico Kevin Tuhoy, em 1948.</p><p>A película gelatinosa só foi inventada no início da década de 1970, quando a empresa Bausch & Lomb concebeu lentes de contato mais confortáveis e maleáveis. Como o processo de manutenção e higienização das lentes de contato era outra chateação para os usuários, vários cientistas começaram a desenvolver um modelo que fosse descartável. Dessa forma, o usuário poderia empregar as lentes de contato somente quando fosse participar de alguma solenidade importante ou tirar uma foto.</p><p>Mesmo concedendo algumas praticidades, as lentes de contato não conseguiram tomar o lugar dos óculos, principalmente para as pessoas que sofrem com a miopia e o astigmatismo. No Brasil, apenas um por cento das pessoas portadoras de problemas oculares fazem uso de lentes de contato.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                }
              ]
            }
          }]
        }
      },
      {
        "name": "/jogos",
        "type": "ScrollScreen",
        "header": "Header",
        "props": {
          "components": [{
            "type" : "Container",
            "styleName":"flexRowWrap",
            "props":{
              "components":[
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A banana tem sementes?</h1><h2>Apesar do que muitos pensam, dependendo da espécie analisada, a banana tem, sim, sementes.</h2><p>Bananeira é o nome dado às plantas agrupadas no gênero conhecido como Musa. Essas espécies possuem folhas grandes e verdes que possuem bainhas que formam um pseudocaule. O caule verdadeiro da bananeira, também chamado de rizoma, é subterrâneo e cresce horizontalmente no solo. A bananeira produz um fruto que é denominado de banana e que é bastante apreciado por toda a população, sendo normalmente consumido in natura.</p>",
                        "htmlContent": "<h1>A banana tem sementes?</h1><h2>Apesar do que muitos pensam, dependendo da espécie analisada, a banana tem, sim, sementes.</h2><p>Bananeira é o nome dado às plantas agrupadas no gênero conhecido como Musa. Essas espécies possuem folhas grandes e verdes que possuem bainhas que formam um pseudocaule. O caule verdadeiro da bananeira, também chamado de rizoma, é subterrâneo e cresce horizontalmente no solo. A bananeira produz um fruto que é denominado de banana e que é bastante apreciado por toda a população, sendo normalmente consumido in natura.</p><p>Apesar de bastante difundido, o fruto não é bem conhecido pela população no que diz respeito, principalmente, à sua estrutura. Muitas pessoas acreditam que os pontinhos pretos que existem no centro da banana são sementes. Entretanto, aquelas estruturas são óvulos não fecundados. A banana cultivada é um fruto partenocárpico, ou seja, que se desenvolve sem que haja fecundação e, portanto, não possui semente.</p><p>O desenvolvimento de bananas sem sementes ocorreu em virtude da seleção desses frutos por produtores que queriam melhorar a qualidade do produto oferecido. Provavelmente, o desenvolvimento inicial desse tipo de fruto ocorreu em razão de alguma mudança genética.</p><p>Vale destacar que ainda hoje é possível encontrar bananas com sementes na natureza. Essas bananas selvagens normalmente desenvolvem sementes em situações de estresse ambiental como uma forma de garantir a sua sobrevivência. Um exemplo de banana que possui sementes é a Musa balbisiana, nativa do sul da Ásia.</p><p>A reprodução da bananeira normalmente é feita por via vegetativa, em que ocorre a separação de brotos da planta-mãe que darão origem a outro indivíduo. Nesse caso, a planta não possui variedade genética, sendo idêntica à planta-mãe. Isso representa um grande problema, uma vez que um espécime com facilidade de contrair doenças dará origem a outro com esse mesmo problema.</p><p>A produção também pode ser feita pelo fracionamento de rizoma, técnica que consiste na plantação de pedaços do caule da planta. Existe ainda a possibilidade de fazer a propagação do vegetal por técnicas de produção de mudas in vitro.</p><p>Curiosidade: As bananas-prata, prata-anã e pacovan são as variedades mais cultivadas no Brasil. De acordo com a Embrapa, essas três espécies são responsáveis por 60% da área cultivada com banana no país.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "Foto",
                      "styleName": "fullWidth485",
                      "props": {
                        "components": [],
                        "url": "https://splashbase.s3.amazonaws.com/splitshire/regular/768x506xSplitShire_IMG_8150-768x506.jpg.pagespeed.ic.KYptVn9mXX.jpg",
                        "href": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>5 filmes para quem ama dinossauros</h1><p>Você ama dinossauros? Então não pode deixar de assistir à franquia Jurassic Park e às animações Em Busca do Vale Encantado e O Bom Dinossauro.</p>",
                        "htmlContent": "<h1>5 filmes para quem ama dinossauros</h1><p>Você ama dinossauros? Então não pode deixar de assistir à franquia Jurassic Park e às animações Em Busca do Vale Encantado e O Bom Dinossauro.</p><p>Os dinossauros, sem dúvidas, mexem com o imaginário de jovens e adultos. Ficamos muitas vezes impressionados com o tamanho de algumas espécies, com a ferocidade de outras e ainda com a ideia de que espécies que dominaram a Terra por tanto tempo simplesmente não estão presentes nos dias atuais.</p><p>Fato é que a indústria do cinema nos ajuda com a tarefa de imaginar como era o mundo quando os dinossauros estavam presentes, mesmo que às vezes as representações contenham erros. A seguir listaremos alguns filmes especialmente selecionados para os amantes dos dinossauros.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção do purgatório</h1><p>Na Idade Média notamos o desenvolvimento de uma série de fatos e experiências históricas que fizeram da Igreja uma das mais poderosas instituições daquela época. A difusão dos preceitos cristãos pela Europa e em outras partes do mundo fez com que os dirigentes desta denominação religiosa interferissem profundamente nos hábitos, concepções e modos de agir de um grande número de pessoas daquela época.</p>",
                        "htmlContent": "<h1>A invenção do purgatório</h1><p>Na Idade Média notamos o desenvolvimento de uma série de fatos e experiências históricas que fizeram da Igreja uma das mais poderosas instituições daquela época. A difusão dos preceitos cristãos pela Europa e em outras partes do mundo fez com que os dirigentes desta denominação religiosa interferissem profundamente nos hábitos, concepções e modos de agir de um grande número de pessoas daquela época.</p><p>Apesar da visibilidade de todo esse processo, não podemos chegar à simplista conclusão de que os clérigos conseguiam fazer com que as pessoas fizessem aquilo que eles bem entendessem. A Igreja influiu na sociedade de sua época, mas também houve situações em que essa poderosa religião também teve de dialogar com as situações e impasses gerados pelos seus próprios seguidores. Para compreendermos tal ponto, podemos tomar a questão da vida após a morte como um interessante exemplo.</p><p>Até o século XII, o cristão estava destinado às glórias e o conforto dos céus ou ao tormento eterno mantido nas profundezas do inferno. A proposição de destinos tão diferentes, fez com que vários fiéis buscassem uma vida predominantemente voltada para a garantia de salvação. Mas como bem sabemos, desde aquele tempo, os pecados atingiam a muitos cristãos e, por isso, pairava uma enorme dúvida sobre qual seria o destino de alguém que não foi nem completamente bom ou ruim.</p><p>Nesse período, é interessante frisarmos que a ordenação social legitimada pela Igreja passava a escapar do seu controle. O mundo medieval antes dividido entre clero, nobreza e servos passava a ganhar a entrada de pessoas que não se ajustavam completamente a esse modelo harmônico dos clérigos medievais. Passando a viver no efervescente ambiente urbano, muitos fiéis e clérigos não tinham meios seguros para dizer se alguém levou ou não uma vida louvável aos olhos de Deus.</p><p>De fato essa discussão era bastante antiga e já tinha presença nos escritos de Santo Agostinho, no século IV. Segundo esse teólogo medieval, o indivíduo que teve uma vida mais inclinada ao pecado seria destinado ao Inferno, mas poderia sair dessa condição através das orações feitas pelos vivos em sua memória. Já aqueles que não foram inteiramente bons passariam por um estágio de purificação que poderia trazê-lo para os céus.</p><p>Até então, o purgatório era compreendido como um processo de salvação espiritual que fugia do que era normalmente convencionado pela Igreja. Segundo a pesquisa de alguns historiadores, a ideia de que o purgatório fosse um “lugar à parte” somente tomou forma entre os séculos XII e XIII. Contudo, engana-se quem acredita que esse terceiro destino no post mortem seja uma proposta originalmente concebida pela cristandade ocidental.</p><p>Os próprios judeus acreditavam que aqueles que não eram nem bons ou maus seriam levados a um lugar onde a pessoa sofreria castigos temporários até que estivessem aptos para viverem no éden. Entre os indianos, os “intermediários” poderiam viver uma série de reencarnações que os levariam até os céus ou ao inferno. Sem dúvida, podemos ver como a própria condição do homem e sua experiência histórica influíram na visão de mundo de várias crenças.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A luz do vaga-lume</h1><p>Vaga-lumes ou pirilampos são insetos das famílias Elateridae, Fengodidae ou Lampyridae muito conhecidos por sua bioluminescência, isto é, sua capacidade de produzir e emitir luz. Essas espécies são dotadas de órgãos fosforescentes na parte inferior de seus segmentos abdominais, responsáveis pelas emissões luminosas.</p>",
                        "htmlContent": "<h1>A luz do vaga-lume</h1><p>Vaga-lumes ou pirilampos são insetos das famílias Elateridae, Fengodidae ou Lampyridae muito conhecidos por sua bioluminescência, isto é, sua capacidade de produzir e emitir luz. Essas espécies são dotadas de órgãos fosforescentes na parte inferior de seus segmentos abdominais, responsáveis pelas emissões luminosas.</p> <p>A bioluminescência é causada pela transformação da energia química em energia luminosa. Esse processo, chamado de 'oxidação biológica', permite que a energia luminosa seja produzida sem que haja a produção de calor. Esse processo ocorre da seguinte maneira: uma molécula de luciferina é oxidada, formando uma molécula de oxiluciferina; quando essa molécula perde sua energia, emite a luz.</p> <p>Esses insetos possuem total controle sobre a emissão de luz, uma vez que o tecido que provoca essa emissão é ligado à traquéia e ao cérebro do vaga-lume. O inseto usa sua bioluminescência para chamar a atenção de seu parceiro ou parceira, por isso, essa habilidade é muito importante no processo de reprodução dessas espécies. Nesse sentido, a iluminação artificial das cidades, que é mais forte, anula a bioluminescência dos vaga-lumes, afetando diretamente o seu processo de reprodução.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p>",
                        "htmlContent": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p><p>Leonardo Da Vinci foi aclamado como um dos maiores expoentes dessa nova tendência. Dono de uma curiosidade voraz, esse italiano atuou em variados campos do conhecimento e da arte. Em alguns de seus manuscritos remanescentes temos a presença de propostas e invenções que falam sobre música, filosofia, botânica, escultura, pintura, urbanismo e engenharia. Atirando para todo o lado, ele acabou sendo um dos primeiros a teorizar uma solução para os problemas de visão.</p><p>Por volta de 1508, ele imaginou a construção de uma lente que, posta na superfície do globo ocular, poderia corrigir os problemas de visão. No século XVII, o filósofo, físico e matemático francês René Descartes foi autor de uma ideia semelhante. No entanto, apesar dos importantes precursores, as populares lentes de contato só foram primordialmente desenvolvidas nos fins do século XIX, pelo fabricante de peças óticas F. E. Muller e o médico suíço Adolf Eugen Fick.</p><p>Os protótipos iniciais não tiveram muito sucesso por conta da limitação tecnológica dessa época. As primeiras lentes de contato foram desenvolvidas por meio do uso do vidro e, por conta da rigidez do material, acabava ferindo os olhos de seus usuários. No ano de 1929, o oftalmologista nova-iorquino William Feinbloom desenvolveu uma nova lente por meio de um material criado através da mistura do vidro e do plástico.</p><p>Apesar do avanço no tipo de material empregado, essas lentes ainda eram bastante rígidas e tinham uma dimensão que causava bastante incômodo aos seus usuários. A partir de então, o aprimoramento do material e do tamanho das lentes permitiu a popularização dos primeiros modelos comerciais. Uma das lentes mais conhecidas dessa época foi criada pelo ótico Kevin Tuhoy, em 1948.</p><p>A película gelatinosa só foi inventada no início da década de 1970, quando a empresa Bausch & Lomb concebeu lentes de contato mais confortáveis e maleáveis. Como o processo de manutenção e higienização das lentes de contato era outra chateação para os usuários, vários cientistas começaram a desenvolver um modelo que fosse descartável. Dessa forma, o usuário poderia empregar as lentes de contato somente quando fosse participar de alguma solenidade importante ou tirar uma foto.</p><p>Mesmo concedendo algumas praticidades, as lentes de contato não conseguiram tomar o lugar dos óculos, principalmente para as pessoas que sofrem com a miopia e o astigmatismo. No Brasil, apenas um por cento das pessoas portadoras de problemas oculares fazem uso de lentes de contato.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "Video",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "video":'https://www.youtube.com/watch?v=DLX62G4lc44',
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p>",
                        "htmlContent": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p><p>Leonardo Da Vinci foi aclamado como um dos maiores expoentes dessa nova tendência. Dono de uma curiosidade voraz, esse italiano atuou em variados campos do conhecimento e da arte. Em alguns de seus manuscritos remanescentes temos a presença de propostas e invenções que falam sobre música, filosofia, botânica, escultura, pintura, urbanismo e engenharia. Atirando para todo o lado, ele acabou sendo um dos primeiros a teorizar uma solução para os problemas de visão.</p><p>Por volta de 1508, ele imaginou a construção de uma lente que, posta na superfície do globo ocular, poderia corrigir os problemas de visão. No século XVII, o filósofo, físico e matemático francês René Descartes foi autor de uma ideia semelhante. No entanto, apesar dos importantes precursores, as populares lentes de contato só foram primordialmente desenvolvidas nos fins do século XIX, pelo fabricante de peças óticas F. E. Muller e o médico suíço Adolf Eugen Fick.</p><p>Os protótipos iniciais não tiveram muito sucesso por conta da limitação tecnológica dessa época. As primeiras lentes de contato foram desenvolvidas por meio do uso do vidro e, por conta da rigidez do material, acabava ferindo os olhos de seus usuários. No ano de 1929, o oftalmologista nova-iorquino William Feinbloom desenvolveu uma nova lente por meio de um material criado através da mistura do vidro e do plástico.</p><p>Apesar do avanço no tipo de material empregado, essas lentes ainda eram bastante rígidas e tinham uma dimensão que causava bastante incômodo aos seus usuários. A partir de então, o aprimoramento do material e do tamanho das lentes permitiu a popularização dos primeiros modelos comerciais. Uma das lentes mais conhecidas dessa época foi criada pelo ótico Kevin Tuhoy, em 1948.</p><p>A película gelatinosa só foi inventada no início da década de 1970, quando a empresa Bausch & Lomb concebeu lentes de contato mais confortáveis e maleáveis. Como o processo de manutenção e higienização das lentes de contato era outra chateação para os usuários, vários cientistas começaram a desenvolver um modelo que fosse descartável. Dessa forma, o usuário poderia empregar as lentes de contato somente quando fosse participar de alguma solenidade importante ou tirar uma foto.</p><p>Mesmo concedendo algumas praticidades, as lentes de contato não conseguiram tomar o lugar dos óculos, principalmente para as pessoas que sofrem com a miopia e o astigmatismo. No Brasil, apenas um por cento das pessoas portadoras de problemas oculares fazem uso de lentes de contato.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                }
              ]
            }
          }]
        }
      },
      {
        "name": "/apps",
        "type": "ScrollScreen",
        "header": "Header",
        "props": {
          "components": [{
            "type" : "Container",
            "styleName":"flexRowWrap",
            "props":{
              "components":[
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A banana tem sementes?</h1><h2>Apesar do que muitos pensam, dependendo da espécie analisada, a banana tem, sim, sementes.</h2><p>Bananeira é o nome dado às plantas agrupadas no gênero conhecido como Musa. Essas espécies possuem folhas grandes e verdes que possuem bainhas que formam um pseudocaule. O caule verdadeiro da bananeira, também chamado de rizoma, é subterrâneo e cresce horizontalmente no solo. A bananeira produz um fruto que é denominado de banana e que é bastante apreciado por toda a população, sendo normalmente consumido in natura.</p>",
                        "htmlContent": "<h1>A banana tem sementes?</h1><h2>Apesar do que muitos pensam, dependendo da espécie analisada, a banana tem, sim, sementes.</h2><p>Bananeira é o nome dado às plantas agrupadas no gênero conhecido como Musa. Essas espécies possuem folhas grandes e verdes que possuem bainhas que formam um pseudocaule. O caule verdadeiro da bananeira, também chamado de rizoma, é subterrâneo e cresce horizontalmente no solo. A bananeira produz um fruto que é denominado de banana e que é bastante apreciado por toda a população, sendo normalmente consumido in natura.</p><p>Apesar de bastante difundido, o fruto não é bem conhecido pela população no que diz respeito, principalmente, à sua estrutura. Muitas pessoas acreditam que os pontinhos pretos que existem no centro da banana são sementes. Entretanto, aquelas estruturas são óvulos não fecundados. A banana cultivada é um fruto partenocárpico, ou seja, que se desenvolve sem que haja fecundação e, portanto, não possui semente.</p><p>O desenvolvimento de bananas sem sementes ocorreu em virtude da seleção desses frutos por produtores que queriam melhorar a qualidade do produto oferecido. Provavelmente, o desenvolvimento inicial desse tipo de fruto ocorreu em razão de alguma mudança genética.</p><p>Vale destacar que ainda hoje é possível encontrar bananas com sementes na natureza. Essas bananas selvagens normalmente desenvolvem sementes em situações de estresse ambiental como uma forma de garantir a sua sobrevivência. Um exemplo de banana que possui sementes é a Musa balbisiana, nativa do sul da Ásia.</p><p>A reprodução da bananeira normalmente é feita por via vegetativa, em que ocorre a separação de brotos da planta-mãe que darão origem a outro indivíduo. Nesse caso, a planta não possui variedade genética, sendo idêntica à planta-mãe. Isso representa um grande problema, uma vez que um espécime com facilidade de contrair doenças dará origem a outro com esse mesmo problema.</p><p>A produção também pode ser feita pelo fracionamento de rizoma, técnica que consiste na plantação de pedaços do caule da planta. Existe ainda a possibilidade de fazer a propagação do vegetal por técnicas de produção de mudas in vitro.</p><p>Curiosidade: As bananas-prata, prata-anã e pacovan são as variedades mais cultivadas no Brasil. De acordo com a Embrapa, essas três espécies são responsáveis por 60% da área cultivada com banana no país.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "Foto",
                      "styleName": "fullWidth485",
                      "props": {
                        "components": [],
                        "url": "https://splashbase.s3.amazonaws.com/splitshire/regular/768x506xSplitShire_IMG_8150-768x506.jpg.pagespeed.ic.KYptVn9mXX.jpg",
                        "href": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>5 filmes para quem ama dinossauros</h1><p>Você ama dinossauros? Então não pode deixar de assistir à franquia Jurassic Park e às animações Em Busca do Vale Encantado e O Bom Dinossauro.</p>",
                        "htmlContent": "<h1>5 filmes para quem ama dinossauros</h1><p>Você ama dinossauros? Então não pode deixar de assistir à franquia Jurassic Park e às animações Em Busca do Vale Encantado e O Bom Dinossauro.</p><p>Os dinossauros, sem dúvidas, mexem com o imaginário de jovens e adultos. Ficamos muitas vezes impressionados com o tamanho de algumas espécies, com a ferocidade de outras e ainda com a ideia de que espécies que dominaram a Terra por tanto tempo simplesmente não estão presentes nos dias atuais.</p><p>Fato é que a indústria do cinema nos ajuda com a tarefa de imaginar como era o mundo quando os dinossauros estavam presentes, mesmo que às vezes as representações contenham erros. A seguir listaremos alguns filmes especialmente selecionados para os amantes dos dinossauros.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção do purgatório</h1><p>Na Idade Média notamos o desenvolvimento de uma série de fatos e experiências históricas que fizeram da Igreja uma das mais poderosas instituições daquela época. A difusão dos preceitos cristãos pela Europa e em outras partes do mundo fez com que os dirigentes desta denominação religiosa interferissem profundamente nos hábitos, concepções e modos de agir de um grande número de pessoas daquela época.</p>",
                        "htmlContent": "<h1>A invenção do purgatório</h1><p>Na Idade Média notamos o desenvolvimento de uma série de fatos e experiências históricas que fizeram da Igreja uma das mais poderosas instituições daquela época. A difusão dos preceitos cristãos pela Europa e em outras partes do mundo fez com que os dirigentes desta denominação religiosa interferissem profundamente nos hábitos, concepções e modos de agir de um grande número de pessoas daquela época.</p><p>Apesar da visibilidade de todo esse processo, não podemos chegar à simplista conclusão de que os clérigos conseguiam fazer com que as pessoas fizessem aquilo que eles bem entendessem. A Igreja influiu na sociedade de sua época, mas também houve situações em que essa poderosa religião também teve de dialogar com as situações e impasses gerados pelos seus próprios seguidores. Para compreendermos tal ponto, podemos tomar a questão da vida após a morte como um interessante exemplo.</p><p>Até o século XII, o cristão estava destinado às glórias e o conforto dos céus ou ao tormento eterno mantido nas profundezas do inferno. A proposição de destinos tão diferentes, fez com que vários fiéis buscassem uma vida predominantemente voltada para a garantia de salvação. Mas como bem sabemos, desde aquele tempo, os pecados atingiam a muitos cristãos e, por isso, pairava uma enorme dúvida sobre qual seria o destino de alguém que não foi nem completamente bom ou ruim.</p><p>Nesse período, é interessante frisarmos que a ordenação social legitimada pela Igreja passava a escapar do seu controle. O mundo medieval antes dividido entre clero, nobreza e servos passava a ganhar a entrada de pessoas que não se ajustavam completamente a esse modelo harmônico dos clérigos medievais. Passando a viver no efervescente ambiente urbano, muitos fiéis e clérigos não tinham meios seguros para dizer se alguém levou ou não uma vida louvável aos olhos de Deus.</p><p>De fato essa discussão era bastante antiga e já tinha presença nos escritos de Santo Agostinho, no século IV. Segundo esse teólogo medieval, o indivíduo que teve uma vida mais inclinada ao pecado seria destinado ao Inferno, mas poderia sair dessa condição através das orações feitas pelos vivos em sua memória. Já aqueles que não foram inteiramente bons passariam por um estágio de purificação que poderia trazê-lo para os céus.</p><p>Até então, o purgatório era compreendido como um processo de salvação espiritual que fugia do que era normalmente convencionado pela Igreja. Segundo a pesquisa de alguns historiadores, a ideia de que o purgatório fosse um “lugar à parte” somente tomou forma entre os séculos XII e XIII. Contudo, engana-se quem acredita que esse terceiro destino no post mortem seja uma proposta originalmente concebida pela cristandade ocidental.</p><p>Os próprios judeus acreditavam que aqueles que não eram nem bons ou maus seriam levados a um lugar onde a pessoa sofreria castigos temporários até que estivessem aptos para viverem no éden. Entre os indianos, os “intermediários” poderiam viver uma série de reencarnações que os levariam até os céus ou ao inferno. Sem dúvida, podemos ver como a própria condição do homem e sua experiência histórica influíram na visão de mundo de várias crenças.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A luz do vaga-lume</h1><p>Vaga-lumes ou pirilampos são insetos das famílias Elateridae, Fengodidae ou Lampyridae muito conhecidos por sua bioluminescência, isto é, sua capacidade de produzir e emitir luz. Essas espécies são dotadas de órgãos fosforescentes na parte inferior de seus segmentos abdominais, responsáveis pelas emissões luminosas.</p>",
                        "htmlContent": "<h1>A luz do vaga-lume</h1><p>Vaga-lumes ou pirilampos são insetos das famílias Elateridae, Fengodidae ou Lampyridae muito conhecidos por sua bioluminescência, isto é, sua capacidade de produzir e emitir luz. Essas espécies são dotadas de órgãos fosforescentes na parte inferior de seus segmentos abdominais, responsáveis pelas emissões luminosas.</p> <p>A bioluminescência é causada pela transformação da energia química em energia luminosa. Esse processo, chamado de 'oxidação biológica', permite que a energia luminosa seja produzida sem que haja a produção de calor. Esse processo ocorre da seguinte maneira: uma molécula de luciferina é oxidada, formando uma molécula de oxiluciferina; quando essa molécula perde sua energia, emite a luz.</p> <p>Esses insetos possuem total controle sobre a emissão de luz, uma vez que o tecido que provoca essa emissão é ligado à traquéia e ao cérebro do vaga-lume. O inseto usa sua bioluminescência para chamar a atenção de seu parceiro ou parceira, por isso, essa habilidade é muito importante no processo de reprodução dessas espécies. Nesse sentido, a iluminação artificial das cidades, que é mais forte, anula a bioluminescência dos vaga-lumes, afetando diretamente o seu processo de reprodução.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p>",
                        "htmlContent": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p><p>Leonardo Da Vinci foi aclamado como um dos maiores expoentes dessa nova tendência. Dono de uma curiosidade voraz, esse italiano atuou em variados campos do conhecimento e da arte. Em alguns de seus manuscritos remanescentes temos a presença de propostas e invenções que falam sobre música, filosofia, botânica, escultura, pintura, urbanismo e engenharia. Atirando para todo o lado, ele acabou sendo um dos primeiros a teorizar uma solução para os problemas de visão.</p><p>Por volta de 1508, ele imaginou a construção de uma lente que, posta na superfície do globo ocular, poderia corrigir os problemas de visão. No século XVII, o filósofo, físico e matemático francês René Descartes foi autor de uma ideia semelhante. No entanto, apesar dos importantes precursores, as populares lentes de contato só foram primordialmente desenvolvidas nos fins do século XIX, pelo fabricante de peças óticas F. E. Muller e o médico suíço Adolf Eugen Fick.</p><p>Os protótipos iniciais não tiveram muito sucesso por conta da limitação tecnológica dessa época. As primeiras lentes de contato foram desenvolvidas por meio do uso do vidro e, por conta da rigidez do material, acabava ferindo os olhos de seus usuários. No ano de 1929, o oftalmologista nova-iorquino William Feinbloom desenvolveu uma nova lente por meio de um material criado através da mistura do vidro e do plástico.</p><p>Apesar do avanço no tipo de material empregado, essas lentes ainda eram bastante rígidas e tinham uma dimensão que causava bastante incômodo aos seus usuários. A partir de então, o aprimoramento do material e do tamanho das lentes permitiu a popularização dos primeiros modelos comerciais. Uma das lentes mais conhecidas dessa época foi criada pelo ótico Kevin Tuhoy, em 1948.</p><p>A película gelatinosa só foi inventada no início da década de 1970, quando a empresa Bausch & Lomb concebeu lentes de contato mais confortáveis e maleáveis. Como o processo de manutenção e higienização das lentes de contato era outra chateação para os usuários, vários cientistas começaram a desenvolver um modelo que fosse descartável. Dessa forma, o usuário poderia empregar as lentes de contato somente quando fosse participar de alguma solenidade importante ou tirar uma foto.</p><p>Mesmo concedendo algumas praticidades, as lentes de contato não conseguiram tomar o lugar dos óculos, principalmente para as pessoas que sofrem com a miopia e o astigmatismo. No Brasil, apenas um por cento das pessoas portadoras de problemas oculares fazem uso de lentes de contato.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "Video",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "video":'https://www.youtube.com/watch?v=DLX62G4lc44',
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p>",
                        "htmlContent": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p><p>Leonardo Da Vinci foi aclamado como um dos maiores expoentes dessa nova tendência. Dono de uma curiosidade voraz, esse italiano atuou em variados campos do conhecimento e da arte. Em alguns de seus manuscritos remanescentes temos a presença de propostas e invenções que falam sobre música, filosofia, botânica, escultura, pintura, urbanismo e engenharia. Atirando para todo o lado, ele acabou sendo um dos primeiros a teorizar uma solução para os problemas de visão.</p><p>Por volta de 1508, ele imaginou a construção de uma lente que, posta na superfície do globo ocular, poderia corrigir os problemas de visão. No século XVII, o filósofo, físico e matemático francês René Descartes foi autor de uma ideia semelhante. No entanto, apesar dos importantes precursores, as populares lentes de contato só foram primordialmente desenvolvidas nos fins do século XIX, pelo fabricante de peças óticas F. E. Muller e o médico suíço Adolf Eugen Fick.</p><p>Os protótipos iniciais não tiveram muito sucesso por conta da limitação tecnológica dessa época. As primeiras lentes de contato foram desenvolvidas por meio do uso do vidro e, por conta da rigidez do material, acabava ferindo os olhos de seus usuários. No ano de 1929, o oftalmologista nova-iorquino William Feinbloom desenvolveu uma nova lente por meio de um material criado através da mistura do vidro e do plástico.</p><p>Apesar do avanço no tipo de material empregado, essas lentes ainda eram bastante rígidas e tinham uma dimensão que causava bastante incômodo aos seus usuários. A partir de então, o aprimoramento do material e do tamanho das lentes permitiu a popularização dos primeiros modelos comerciais. Uma das lentes mais conhecidas dessa época foi criada pelo ótico Kevin Tuhoy, em 1948.</p><p>A película gelatinosa só foi inventada no início da década de 1970, quando a empresa Bausch & Lomb concebeu lentes de contato mais confortáveis e maleáveis. Como o processo de manutenção e higienização das lentes de contato era outra chateação para os usuários, vários cientistas começaram a desenvolver um modelo que fosse descartável. Dessa forma, o usuário poderia empregar as lentes de contato somente quando fosse participar de alguma solenidade importante ou tirar uma foto.</p><p>Mesmo concedendo algumas praticidades, as lentes de contato não conseguiram tomar o lugar dos óculos, principalmente para as pessoas que sofrem com a miopia e o astigmatismo. No Brasil, apenas um por cento das pessoas portadoras de problemas oculares fazem uso de lentes de contato.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                }
              ]
            }
          }]
        }
      },
      {
        "name": "/filmes",
        "type": "ScrollScreen",
        "header": "Header",
        "props": {
          "components": [{
            "type" : "Container",
            "styleName":"flexRowWrap",
            "props":{
              "components":[
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A banana tem sementes?</h1><h2>Apesar do que muitos pensam, dependendo da espécie analisada, a banana tem, sim, sementes.</h2><p>Bananeira é o nome dado às plantas agrupadas no gênero conhecido como Musa. Essas espécies possuem folhas grandes e verdes que possuem bainhas que formam um pseudocaule. O caule verdadeiro da bananeira, também chamado de rizoma, é subterrâneo e cresce horizontalmente no solo. A bananeira produz um fruto que é denominado de banana e que é bastante apreciado por toda a população, sendo normalmente consumido in natura.</p>",
                        "htmlContent": "<h1>A banana tem sementes?</h1><h2>Apesar do que muitos pensam, dependendo da espécie analisada, a banana tem, sim, sementes.</h2><p>Bananeira é o nome dado às plantas agrupadas no gênero conhecido como Musa. Essas espécies possuem folhas grandes e verdes que possuem bainhas que formam um pseudocaule. O caule verdadeiro da bananeira, também chamado de rizoma, é subterrâneo e cresce horizontalmente no solo. A bananeira produz um fruto que é denominado de banana e que é bastante apreciado por toda a população, sendo normalmente consumido in natura.</p><p>Apesar de bastante difundido, o fruto não é bem conhecido pela população no que diz respeito, principalmente, à sua estrutura. Muitas pessoas acreditam que os pontinhos pretos que existem no centro da banana são sementes. Entretanto, aquelas estruturas são óvulos não fecundados. A banana cultivada é um fruto partenocárpico, ou seja, que se desenvolve sem que haja fecundação e, portanto, não possui semente.</p><p>O desenvolvimento de bananas sem sementes ocorreu em virtude da seleção desses frutos por produtores que queriam melhorar a qualidade do produto oferecido. Provavelmente, o desenvolvimento inicial desse tipo de fruto ocorreu em razão de alguma mudança genética.</p><p>Vale destacar que ainda hoje é possível encontrar bananas com sementes na natureza. Essas bananas selvagens normalmente desenvolvem sementes em situações de estresse ambiental como uma forma de garantir a sua sobrevivência. Um exemplo de banana que possui sementes é a Musa balbisiana, nativa do sul da Ásia.</p><p>A reprodução da bananeira normalmente é feita por via vegetativa, em que ocorre a separação de brotos da planta-mãe que darão origem a outro indivíduo. Nesse caso, a planta não possui variedade genética, sendo idêntica à planta-mãe. Isso representa um grande problema, uma vez que um espécime com facilidade de contrair doenças dará origem a outro com esse mesmo problema.</p><p>A produção também pode ser feita pelo fracionamento de rizoma, técnica que consiste na plantação de pedaços do caule da planta. Existe ainda a possibilidade de fazer a propagação do vegetal por técnicas de produção de mudas in vitro.</p><p>Curiosidade: As bananas-prata, prata-anã e pacovan são as variedades mais cultivadas no Brasil. De acordo com a Embrapa, essas três espécies são responsáveis por 60% da área cultivada com banana no país.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "Foto",
                      "styleName": "fullWidth485",
                      "props": {
                        "components": [],
                        "url": "https://splashbase.s3.amazonaws.com/splitshire/regular/768x506xSplitShire_IMG_8150-768x506.jpg.pagespeed.ic.KYptVn9mXX.jpg",
                        "href": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>5 filmes para quem ama dinossauros</h1><p>Você ama dinossauros? Então não pode deixar de assistir à franquia Jurassic Park e às animações Em Busca do Vale Encantado e O Bom Dinossauro.</p>",
                        "htmlContent": "<h1>5 filmes para quem ama dinossauros</h1><p>Você ama dinossauros? Então não pode deixar de assistir à franquia Jurassic Park e às animações Em Busca do Vale Encantado e O Bom Dinossauro.</p><p>Os dinossauros, sem dúvidas, mexem com o imaginário de jovens e adultos. Ficamos muitas vezes impressionados com o tamanho de algumas espécies, com a ferocidade de outras e ainda com a ideia de que espécies que dominaram a Terra por tanto tempo simplesmente não estão presentes nos dias atuais.</p><p>Fato é que a indústria do cinema nos ajuda com a tarefa de imaginar como era o mundo quando os dinossauros estavam presentes, mesmo que às vezes as representações contenham erros. A seguir listaremos alguns filmes especialmente selecionados para os amantes dos dinossauros.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção do purgatório</h1><p>Na Idade Média notamos o desenvolvimento de uma série de fatos e experiências históricas que fizeram da Igreja uma das mais poderosas instituições daquela época. A difusão dos preceitos cristãos pela Europa e em outras partes do mundo fez com que os dirigentes desta denominação religiosa interferissem profundamente nos hábitos, concepções e modos de agir de um grande número de pessoas daquela época.</p>",
                        "htmlContent": "<h1>A invenção do purgatório</h1><p>Na Idade Média notamos o desenvolvimento de uma série de fatos e experiências históricas que fizeram da Igreja uma das mais poderosas instituições daquela época. A difusão dos preceitos cristãos pela Europa e em outras partes do mundo fez com que os dirigentes desta denominação religiosa interferissem profundamente nos hábitos, concepções e modos de agir de um grande número de pessoas daquela época.</p><p>Apesar da visibilidade de todo esse processo, não podemos chegar à simplista conclusão de que os clérigos conseguiam fazer com que as pessoas fizessem aquilo que eles bem entendessem. A Igreja influiu na sociedade de sua época, mas também houve situações em que essa poderosa religião também teve de dialogar com as situações e impasses gerados pelos seus próprios seguidores. Para compreendermos tal ponto, podemos tomar a questão da vida após a morte como um interessante exemplo.</p><p>Até o século XII, o cristão estava destinado às glórias e o conforto dos céus ou ao tormento eterno mantido nas profundezas do inferno. A proposição de destinos tão diferentes, fez com que vários fiéis buscassem uma vida predominantemente voltada para a garantia de salvação. Mas como bem sabemos, desde aquele tempo, os pecados atingiam a muitos cristãos e, por isso, pairava uma enorme dúvida sobre qual seria o destino de alguém que não foi nem completamente bom ou ruim.</p><p>Nesse período, é interessante frisarmos que a ordenação social legitimada pela Igreja passava a escapar do seu controle. O mundo medieval antes dividido entre clero, nobreza e servos passava a ganhar a entrada de pessoas que não se ajustavam completamente a esse modelo harmônico dos clérigos medievais. Passando a viver no efervescente ambiente urbano, muitos fiéis e clérigos não tinham meios seguros para dizer se alguém levou ou não uma vida louvável aos olhos de Deus.</p><p>De fato essa discussão era bastante antiga e já tinha presença nos escritos de Santo Agostinho, no século IV. Segundo esse teólogo medieval, o indivíduo que teve uma vida mais inclinada ao pecado seria destinado ao Inferno, mas poderia sair dessa condição através das orações feitas pelos vivos em sua memória. Já aqueles que não foram inteiramente bons passariam por um estágio de purificação que poderia trazê-lo para os céus.</p><p>Até então, o purgatório era compreendido como um processo de salvação espiritual que fugia do que era normalmente convencionado pela Igreja. Segundo a pesquisa de alguns historiadores, a ideia de que o purgatório fosse um “lugar à parte” somente tomou forma entre os séculos XII e XIII. Contudo, engana-se quem acredita que esse terceiro destino no post mortem seja uma proposta originalmente concebida pela cristandade ocidental.</p><p>Os próprios judeus acreditavam que aqueles que não eram nem bons ou maus seriam levados a um lugar onde a pessoa sofreria castigos temporários até que estivessem aptos para viverem no éden. Entre os indianos, os “intermediários” poderiam viver uma série de reencarnações que os levariam até os céus ou ao inferno. Sem dúvida, podemos ver como a própria condição do homem e sua experiência histórica influíram na visão de mundo de várias crenças.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A luz do vaga-lume</h1><p>Vaga-lumes ou pirilampos são insetos das famílias Elateridae, Fengodidae ou Lampyridae muito conhecidos por sua bioluminescência, isto é, sua capacidade de produzir e emitir luz. Essas espécies são dotadas de órgãos fosforescentes na parte inferior de seus segmentos abdominais, responsáveis pelas emissões luminosas.</p>",
                        "htmlContent": "<h1>A luz do vaga-lume</h1><p>Vaga-lumes ou pirilampos são insetos das famílias Elateridae, Fengodidae ou Lampyridae muito conhecidos por sua bioluminescência, isto é, sua capacidade de produzir e emitir luz. Essas espécies são dotadas de órgãos fosforescentes na parte inferior de seus segmentos abdominais, responsáveis pelas emissões luminosas.</p> <p>A bioluminescência é causada pela transformação da energia química em energia luminosa. Esse processo, chamado de 'oxidação biológica', permite que a energia luminosa seja produzida sem que haja a produção de calor. Esse processo ocorre da seguinte maneira: uma molécula de luciferina é oxidada, formando uma molécula de oxiluciferina; quando essa molécula perde sua energia, emite a luz.</p> <p>Esses insetos possuem total controle sobre a emissão de luz, uma vez que o tecido que provoca essa emissão é ligado à traquéia e ao cérebro do vaga-lume. O inseto usa sua bioluminescência para chamar a atenção de seu parceiro ou parceira, por isso, essa habilidade é muito importante no processo de reprodução dessas espécies. Nesse sentido, a iluminação artificial das cidades, que é mais forte, anula a bioluminescência dos vaga-lumes, afetando diretamente o seu processo de reprodução.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p>",
                        "htmlContent": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p><p>Leonardo Da Vinci foi aclamado como um dos maiores expoentes dessa nova tendência. Dono de uma curiosidade voraz, esse italiano atuou em variados campos do conhecimento e da arte. Em alguns de seus manuscritos remanescentes temos a presença de propostas e invenções que falam sobre música, filosofia, botânica, escultura, pintura, urbanismo e engenharia. Atirando para todo o lado, ele acabou sendo um dos primeiros a teorizar uma solução para os problemas de visão.</p><p>Por volta de 1508, ele imaginou a construção de uma lente que, posta na superfície do globo ocular, poderia corrigir os problemas de visão. No século XVII, o filósofo, físico e matemático francês René Descartes foi autor de uma ideia semelhante. No entanto, apesar dos importantes precursores, as populares lentes de contato só foram primordialmente desenvolvidas nos fins do século XIX, pelo fabricante de peças óticas F. E. Muller e o médico suíço Adolf Eugen Fick.</p><p>Os protótipos iniciais não tiveram muito sucesso por conta da limitação tecnológica dessa época. As primeiras lentes de contato foram desenvolvidas por meio do uso do vidro e, por conta da rigidez do material, acabava ferindo os olhos de seus usuários. No ano de 1929, o oftalmologista nova-iorquino William Feinbloom desenvolveu uma nova lente por meio de um material criado através da mistura do vidro e do plástico.</p><p>Apesar do avanço no tipo de material empregado, essas lentes ainda eram bastante rígidas e tinham uma dimensão que causava bastante incômodo aos seus usuários. A partir de então, o aprimoramento do material e do tamanho das lentes permitiu a popularização dos primeiros modelos comerciais. Uma das lentes mais conhecidas dessa época foi criada pelo ótico Kevin Tuhoy, em 1948.</p><p>A película gelatinosa só foi inventada no início da década de 1970, quando a empresa Bausch & Lomb concebeu lentes de contato mais confortáveis e maleáveis. Como o processo de manutenção e higienização das lentes de contato era outra chateação para os usuários, vários cientistas começaram a desenvolver um modelo que fosse descartável. Dessa forma, o usuário poderia empregar as lentes de contato somente quando fosse participar de alguma solenidade importante ou tirar uma foto.</p><p>Mesmo concedendo algumas praticidades, as lentes de contato não conseguiram tomar o lugar dos óculos, principalmente para as pessoas que sofrem com a miopia e o astigmatismo. No Brasil, apenas um por cento das pessoas portadoras de problemas oculares fazem uso de lentes de contato.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "Video",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "video":'https://www.youtube.com/watch?v=DLX62G4lc44',
                      }
                    }]
                  }
                }
              ]
            }
          }]
        }
      },
      {
        "name": "/livros",
        "type": "ScrollScreen",
        "header": "Header",
        "props": {
          "components": [{
            "type" : "Container",
            "styleName":"flexRowWrap",
            "props":{
              "components":[
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A banana tem sementes?</h1><h2>Apesar do que muitos pensam, dependendo da espécie analisada, a banana tem, sim, sementes.</h2><p>Bananeira é o nome dado às plantas agrupadas no gênero conhecido como Musa. Essas espécies possuem folhas grandes e verdes que possuem bainhas que formam um pseudocaule. O caule verdadeiro da bananeira, também chamado de rizoma, é subterrâneo e cresce horizontalmente no solo. A bananeira produz um fruto que é denominado de banana e que é bastante apreciado por toda a população, sendo normalmente consumido in natura.</p>",
                        "htmlContent": "<h1>A banana tem sementes?</h1><h2>Apesar do que muitos pensam, dependendo da espécie analisada, a banana tem, sim, sementes.</h2><p>Bananeira é o nome dado às plantas agrupadas no gênero conhecido como Musa. Essas espécies possuem folhas grandes e verdes que possuem bainhas que formam um pseudocaule. O caule verdadeiro da bananeira, também chamado de rizoma, é subterrâneo e cresce horizontalmente no solo. A bananeira produz um fruto que é denominado de banana e que é bastante apreciado por toda a população, sendo normalmente consumido in natura.</p><p>Apesar de bastante difundido, o fruto não é bem conhecido pela população no que diz respeito, principalmente, à sua estrutura. Muitas pessoas acreditam que os pontinhos pretos que existem no centro da banana são sementes. Entretanto, aquelas estruturas são óvulos não fecundados. A banana cultivada é um fruto partenocárpico, ou seja, que se desenvolve sem que haja fecundação e, portanto, não possui semente.</p><p>O desenvolvimento de bananas sem sementes ocorreu em virtude da seleção desses frutos por produtores que queriam melhorar a qualidade do produto oferecido. Provavelmente, o desenvolvimento inicial desse tipo de fruto ocorreu em razão de alguma mudança genética.</p><p>Vale destacar que ainda hoje é possível encontrar bananas com sementes na natureza. Essas bananas selvagens normalmente desenvolvem sementes em situações de estresse ambiental como uma forma de garantir a sua sobrevivência. Um exemplo de banana que possui sementes é a Musa balbisiana, nativa do sul da Ásia.</p><p>A reprodução da bananeira normalmente é feita por via vegetativa, em que ocorre a separação de brotos da planta-mãe que darão origem a outro indivíduo. Nesse caso, a planta não possui variedade genética, sendo idêntica à planta-mãe. Isso representa um grande problema, uma vez que um espécime com facilidade de contrair doenças dará origem a outro com esse mesmo problema.</p><p>A produção também pode ser feita pelo fracionamento de rizoma, técnica que consiste na plantação de pedaços do caule da planta. Existe ainda a possibilidade de fazer a propagação do vegetal por técnicas de produção de mudas in vitro.</p><p>Curiosidade: As bananas-prata, prata-anã e pacovan são as variedades mais cultivadas no Brasil. De acordo com a Embrapa, essas três espécies são responsáveis por 60% da área cultivada com banana no país.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "Foto",
                      "styleName": "fullWidth485",
                      "props": {
                        "components": [],
                        "url": "https://splashbase.s3.amazonaws.com/splitshire/regular/768x506xSplitShire_IMG_8150-768x506.jpg.pagespeed.ic.KYptVn9mXX.jpg",
                        "href": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>5 filmes para quem ama dinossauros</h1><p>Você ama dinossauros? Então não pode deixar de assistir à franquia Jurassic Park e às animações Em Busca do Vale Encantado e O Bom Dinossauro.</p>",
                        "htmlContent": "<h1>5 filmes para quem ama dinossauros</h1><p>Você ama dinossauros? Então não pode deixar de assistir à franquia Jurassic Park e às animações Em Busca do Vale Encantado e O Bom Dinossauro.</p><p>Os dinossauros, sem dúvidas, mexem com o imaginário de jovens e adultos. Ficamos muitas vezes impressionados com o tamanho de algumas espécies, com a ferocidade de outras e ainda com a ideia de que espécies que dominaram a Terra por tanto tempo simplesmente não estão presentes nos dias atuais.</p><p>Fato é que a indústria do cinema nos ajuda com a tarefa de imaginar como era o mundo quando os dinossauros estavam presentes, mesmo que às vezes as representações contenham erros. A seguir listaremos alguns filmes especialmente selecionados para os amantes dos dinossauros.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção do purgatório</h1><p>Na Idade Média notamos o desenvolvimento de uma série de fatos e experiências históricas que fizeram da Igreja uma das mais poderosas instituições daquela época. A difusão dos preceitos cristãos pela Europa e em outras partes do mundo fez com que os dirigentes desta denominação religiosa interferissem profundamente nos hábitos, concepções e modos de agir de um grande número de pessoas daquela época.</p>",
                        "htmlContent": "<h1>A invenção do purgatório</h1><p>Na Idade Média notamos o desenvolvimento de uma série de fatos e experiências históricas que fizeram da Igreja uma das mais poderosas instituições daquela época. A difusão dos preceitos cristãos pela Europa e em outras partes do mundo fez com que os dirigentes desta denominação religiosa interferissem profundamente nos hábitos, concepções e modos de agir de um grande número de pessoas daquela época.</p><p>Apesar da visibilidade de todo esse processo, não podemos chegar à simplista conclusão de que os clérigos conseguiam fazer com que as pessoas fizessem aquilo que eles bem entendessem. A Igreja influiu na sociedade de sua época, mas também houve situações em que essa poderosa religião também teve de dialogar com as situações e impasses gerados pelos seus próprios seguidores. Para compreendermos tal ponto, podemos tomar a questão da vida após a morte como um interessante exemplo.</p><p>Até o século XII, o cristão estava destinado às glórias e o conforto dos céus ou ao tormento eterno mantido nas profundezas do inferno. A proposição de destinos tão diferentes, fez com que vários fiéis buscassem uma vida predominantemente voltada para a garantia de salvação. Mas como bem sabemos, desde aquele tempo, os pecados atingiam a muitos cristãos e, por isso, pairava uma enorme dúvida sobre qual seria o destino de alguém que não foi nem completamente bom ou ruim.</p><p>Nesse período, é interessante frisarmos que a ordenação social legitimada pela Igreja passava a escapar do seu controle. O mundo medieval antes dividido entre clero, nobreza e servos passava a ganhar a entrada de pessoas que não se ajustavam completamente a esse modelo harmônico dos clérigos medievais. Passando a viver no efervescente ambiente urbano, muitos fiéis e clérigos não tinham meios seguros para dizer se alguém levou ou não uma vida louvável aos olhos de Deus.</p><p>De fato essa discussão era bastante antiga e já tinha presença nos escritos de Santo Agostinho, no século IV. Segundo esse teólogo medieval, o indivíduo que teve uma vida mais inclinada ao pecado seria destinado ao Inferno, mas poderia sair dessa condição através das orações feitas pelos vivos em sua memória. Já aqueles que não foram inteiramente bons passariam por um estágio de purificação que poderia trazê-lo para os céus.</p><p>Até então, o purgatório era compreendido como um processo de salvação espiritual que fugia do que era normalmente convencionado pela Igreja. Segundo a pesquisa de alguns historiadores, a ideia de que o purgatório fosse um “lugar à parte” somente tomou forma entre os séculos XII e XIII. Contudo, engana-se quem acredita que esse terceiro destino no post mortem seja uma proposta originalmente concebida pela cristandade ocidental.</p><p>Os próprios judeus acreditavam que aqueles que não eram nem bons ou maus seriam levados a um lugar onde a pessoa sofreria castigos temporários até que estivessem aptos para viverem no éden. Entre os indianos, os “intermediários” poderiam viver uma série de reencarnações que os levariam até os céus ou ao inferno. Sem dúvida, podemos ver como a própria condição do homem e sua experiência histórica influíram na visão de mundo de várias crenças.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A luz do vaga-lume</h1><p>Vaga-lumes ou pirilampos são insetos das famílias Elateridae, Fengodidae ou Lampyridae muito conhecidos por sua bioluminescência, isto é, sua capacidade de produzir e emitir luz. Essas espécies são dotadas de órgãos fosforescentes na parte inferior de seus segmentos abdominais, responsáveis pelas emissões luminosas.</p>",
                        "htmlContent": "<h1>A luz do vaga-lume</h1><p>Vaga-lumes ou pirilampos são insetos das famílias Elateridae, Fengodidae ou Lampyridae muito conhecidos por sua bioluminescência, isto é, sua capacidade de produzir e emitir luz. Essas espécies são dotadas de órgãos fosforescentes na parte inferior de seus segmentos abdominais, responsáveis pelas emissões luminosas.</p> <p>A bioluminescência é causada pela transformação da energia química em energia luminosa. Esse processo, chamado de 'oxidação biológica', permite que a energia luminosa seja produzida sem que haja a produção de calor. Esse processo ocorre da seguinte maneira: uma molécula de luciferina é oxidada, formando uma molécula de oxiluciferina; quando essa molécula perde sua energia, emite a luz.</p> <p>Esses insetos possuem total controle sobre a emissão de luz, uma vez que o tecido que provoca essa emissão é ligado à traquéia e ao cérebro do vaga-lume. O inseto usa sua bioluminescência para chamar a atenção de seu parceiro ou parceira, por isso, essa habilidade é muito importante no processo de reprodução dessas espécies. Nesse sentido, a iluminação artificial das cidades, que é mais forte, anula a bioluminescência dos vaga-lumes, afetando diretamente o seu processo de reprodução.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p>",
                        "htmlContent": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p><p>Leonardo Da Vinci foi aclamado como um dos maiores expoentes dessa nova tendência. Dono de uma curiosidade voraz, esse italiano atuou em variados campos do conhecimento e da arte. Em alguns de seus manuscritos remanescentes temos a presença de propostas e invenções que falam sobre música, filosofia, botânica, escultura, pintura, urbanismo e engenharia. Atirando para todo o lado, ele acabou sendo um dos primeiros a teorizar uma solução para os problemas de visão.</p><p>Por volta de 1508, ele imaginou a construção de uma lente que, posta na superfície do globo ocular, poderia corrigir os problemas de visão. No século XVII, o filósofo, físico e matemático francês René Descartes foi autor de uma ideia semelhante. No entanto, apesar dos importantes precursores, as populares lentes de contato só foram primordialmente desenvolvidas nos fins do século XIX, pelo fabricante de peças óticas F. E. Muller e o médico suíço Adolf Eugen Fick.</p><p>Os protótipos iniciais não tiveram muito sucesso por conta da limitação tecnológica dessa época. As primeiras lentes de contato foram desenvolvidas por meio do uso do vidro e, por conta da rigidez do material, acabava ferindo os olhos de seus usuários. No ano de 1929, o oftalmologista nova-iorquino William Feinbloom desenvolveu uma nova lente por meio de um material criado através da mistura do vidro e do plástico.</p><p>Apesar do avanço no tipo de material empregado, essas lentes ainda eram bastante rígidas e tinham uma dimensão que causava bastante incômodo aos seus usuários. A partir de então, o aprimoramento do material e do tamanho das lentes permitiu a popularização dos primeiros modelos comerciais. Uma das lentes mais conhecidas dessa época foi criada pelo ótico Kevin Tuhoy, em 1948.</p><p>A película gelatinosa só foi inventada no início da década de 1970, quando a empresa Bausch & Lomb concebeu lentes de contato mais confortáveis e maleáveis. Como o processo de manutenção e higienização das lentes de contato era outra chateação para os usuários, vários cientistas começaram a desenvolver um modelo que fosse descartável. Dessa forma, o usuário poderia empregar as lentes de contato somente quando fosse participar de alguma solenidade importante ou tirar uma foto.</p><p>Mesmo concedendo algumas praticidades, as lentes de contato não conseguiram tomar o lugar dos óculos, principalmente para as pessoas que sofrem com a miopia e o astigmatismo. No Brasil, apenas um por cento das pessoas portadoras de problemas oculares fazem uso de lentes de contato.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "Video",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "video":'https://www.youtube.com/watch?v=DLX62G4lc44',
                      }
                    }]
                  }
                },
                {
                  "type": "Container",
                  "styleName": "cards",
                  "props": {
                    "components": [{
                      "type": "HtmlView",
                      "styleName": "",
                      "props": {
                        "components": [],
                        "abstract": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p>",
                        "htmlContent": "<h1>A invenção das lentes de contato</h1><p>Entre os séculos XIV e XVI, as mudanças políticas e econômicas que se alastraram pela Europa foram acompanhadas por um novo movimento de natureza cultural e científica. O Renascimento, ou movimento renascentista, irrompeu a predominância dos valores eclesiásticos para estabelecer a preocupação expressa com os assuntos considerados terrenos. Dessa forma, vários pensadores, estudiosos e artistas voltaram a sua atenção para questões de cunho humano.</p><p>Leonardo Da Vinci foi aclamado como um dos maiores expoentes dessa nova tendência. Dono de uma curiosidade voraz, esse italiano atuou em variados campos do conhecimento e da arte. Em alguns de seus manuscritos remanescentes temos a presença de propostas e invenções que falam sobre música, filosofia, botânica, escultura, pintura, urbanismo e engenharia. Atirando para todo o lado, ele acabou sendo um dos primeiros a teorizar uma solução para os problemas de visão.</p><p>Por volta de 1508, ele imaginou a construção de uma lente que, posta na superfície do globo ocular, poderia corrigir os problemas de visão. No século XVII, o filósofo, físico e matemático francês René Descartes foi autor de uma ideia semelhante. No entanto, apesar dos importantes precursores, as populares lentes de contato só foram primordialmente desenvolvidas nos fins do século XIX, pelo fabricante de peças óticas F. E. Muller e o médico suíço Adolf Eugen Fick.</p><p>Os protótipos iniciais não tiveram muito sucesso por conta da limitação tecnológica dessa época. As primeiras lentes de contato foram desenvolvidas por meio do uso do vidro e, por conta da rigidez do material, acabava ferindo os olhos de seus usuários. No ano de 1929, o oftalmologista nova-iorquino William Feinbloom desenvolveu uma nova lente por meio de um material criado através da mistura do vidro e do plástico.</p><p>Apesar do avanço no tipo de material empregado, essas lentes ainda eram bastante rígidas e tinham uma dimensão que causava bastante incômodo aos seus usuários. A partir de então, o aprimoramento do material e do tamanho das lentes permitiu a popularização dos primeiros modelos comerciais. Uma das lentes mais conhecidas dessa época foi criada pelo ótico Kevin Tuhoy, em 1948.</p><p>A película gelatinosa só foi inventada no início da década de 1970, quando a empresa Bausch & Lomb concebeu lentes de contato mais confortáveis e maleáveis. Como o processo de manutenção e higienização das lentes de contato era outra chateação para os usuários, vários cientistas começaram a desenvolver um modelo que fosse descartável. Dessa forma, o usuário poderia empregar as lentes de contato somente quando fosse participar de alguma solenidade importante ou tirar uma foto.</p><p>Mesmo concedendo algumas praticidades, as lentes de contato não conseguiram tomar o lugar dos óculos, principalmente para as pessoas que sofrem com a miopia e o astigmatismo. No Brasil, apenas um por cento das pessoas portadoras de problemas oculares fazem uso de lentes de contato.</p>",
                        "textStyle": "",
                        "transformTextAction": ""
                      }
                    }]
                  }
                }
              ]
            }
          }]
        }
      },
      {
        "name": "/perfil",
        "type": "ScrollScreen",
        "header": "Header",
        "props": {
          "components": [
            {
              "type": "Container",
              "styleName": "perfil",
              "props": {
                "components": [
                  {
                    "type": "Image",
                    "styleName": "perfil",
                    "props": {
                      "components": [],
                      "url": "https://avatars.githubusercontent.com/jodelismarko",
                      "href": ""
                    }
                  }
                ]
              }
            },
            {
              "type": "HtmlView",
              "styleName": "",
              "props": {
                "components": [],
                "abstract": "<h1>Apoio ao cliente</h1><h2>Quer consultar, simular, esclarecer ou subscrever?</h2><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p>",
                "htmlContent": "<h1>Apoio ao cliente</h1><h2>Quer consultar, simular, esclarecer ou subscrever?</h2><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Por isso, criámos sete Canais Oney Contacto para estar sempre a par do seu mundo Oney e para que possa tomar decisões conscientes.</p><p>Por isso, criámos sete Canais Oney Contacto para estar sempre a par do seu mundo Oney e para que possa tomar decisões conscientes.</p>",
                "textStyle": "",
                "transformTextAction": ""
              }
            },
            {
              "type": "HtmlView",
              "styleName": "",
              "props": {
                "components": [],
                "abstract": "<h1>Apoio ao cliente</h1><h2>Quer consultar, simular, esclarecer ou subscrever?</h2><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p>",
                "htmlContent": "<h1>Apoio ao cliente</h1><h2>Quer consultar, simular, esclarecer ou subscrever?</h2><p>Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui! Seja o que for que precisa ou esteja onde estiver, conte sempre connosco, estamos sempre disponíveis para si, aqui!</p>",
                "textStyle": "",
                "transformTextAction": ""
              }
            }
          ]
        }
      }
    ],
    "headers": [
      {
        "name": "Header",
        "type": "Container",
        "styleName": "header",
        "props": {
          "components": [
            {
              "type": "Container",
              "styleName": "flexRowSpaceBetween",
              "props": {
                "components": [
                  {
                    "type": "LinkButton",
                    "styleName": "navigationLink",
                    "props": {
                      "components": [],
                      "image": "https://s1.gifyu.com/images/baseline_home_white_18dp.png",
                      "href": "/pt-PT/",
                      "title": "Home",
                      "linkType": "internal"
                    }
                  },
                  {
                    "type": "LinkButton",
                    "styleName": "navigationLink",
                    "props": {
                      "components": [],
                      "image": "https://s1.gifyu.com/images/baseline_games_white_18dp.png",
                      "href": "/pt-PT/jogos",
                      "title": "Jogos",
                      "linkType": "internal"
                    }
                  },
                  {
                    "type": "LinkButton",
                    "styleName": "navigationLink",
                    "props": {
                      "components": [],
                      "image": "https://s1.gifyu.com/images/baseline_apps_white_18dp.png",
                      "href": "/pt-PT/apps",
                      "title": "Apps",
                      "linkType": "internal"
                    }
                  },
                  {
                    "type": "LinkButton",
                    "styleName": "navigationLink",
                    "props": {
                      "components": [],
                      "image": "https://s1.gifyu.com/images/baseline_movie_white_18dp.png",
                      "href": "/pt-PT/filmes",
                      "title": "Filmes",
                      "linkType": "internal"
                    }
                  },
                  {
                    "type": "LinkButton",
                    "styleName": "navigationLink",
                    "props": {
                      "components": [],
                      "image": "https://s1.gifyu.com/images/baseline_menu_book_white_18dp.png",
                      "href": "/pt-PT/livros",
                      "title": "Livros",
                      "linkType": "internal"
                    }
                  },
                  {
                    "type": "LinkButton",
                    "styleName": "navigationLinkAvatar",
                    "props": {
                      "components": [],
                      "image": "https://avatars.githubusercontent.com/jodelismarko",
                      "href": "/pt-PT/perfil",
                      "title": "Marko",
                      "linkType": "internal"
                    }
                  }
                ]
              }
            }
          ]
        }
      }
    ]
  }