import axios from "axios";

export class RestClient {
    constructor({ url, user, pwd, defaultParameters }) {
        this.credentials = user && pwd ? {
            user,
            pwd
        } : null;

        this.params = defaultParameters;

        this.client = axios.create({
            baseURL: url,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
            withCredentials: user && pwd ? true : false
        });
    }

    async get(url) {
        let data = null;

        if (!data) {
            data = await this.request({
                method: "get",
                url,
                params: this.params
            });
        }

        return data.hasOwnProperty("Data") ? data.Data : data;
    }

    post(url, data) {
        return this.request({ method: "POST", url, data });
    }

    async request(config) {
        try {
            let response = await this.client.request(config);
            return response.data;
        } catch (error) {
            // console.error(error);
            throw error;
        }
    }
}
