/**
 * @author João Liliu
 * @description User context
 * @global
 * @typedef {Object} UserContextValues
 * @property {Boolean} authenticated
 * @property {Object} authData
 * @property {Object} user
 * @property {Function} signIn
 * @property {Function} signOut
 * @property {Function} updateUser
 * @property {Function} getUserValue
 * @property {Function} getActiveContract
 */

import { createContext, useContext } from "react";

const UserContext = createContext({});

/**
 * @description Hook to use UserContext
 * @returns {UserContextValues} UserContext
 */
export function useUserContext() {
    return useContext(UserContext);
}



export default UserContext;