import React, { useState, useEffect } from "react";
import { JsUtils } from "../../../core/utils";
import UserContext from "./index";

const __USER_OFFLINE__  = true;

export default function ({ children }) {
    const [authData, setAuth] = useState(null);
    const [user, setUser] = useState(null);

    useEffect(() => {
        return signOut;
    }, []);

    useEffect(() => {
        if (user)
            user.activeContract = getActiveContract();
    }, [user]);

    function signIn(auth, userData) {
        setAuth(auth);
        setUser(userData);
    }

    function signOut() {
        setAuth(null);
        setUser(null);
    }

    function getUserValue(name) {
        return JsUtils.getDeepValue(user, name);
    }

    function getActiveContract() {
        return user.contracts && user.contracts.find(x => x.isCurrent);
    }

    function updateUser(newUser) {
        setUser({ ...newUser });
    }

    return (
        <UserContext.Provider value={{
            authenticated: !!user,
            authData,
            user,
            updateUser,
            signIn,
            signOut,
            getUserValue,
            getActiveContract
        }}>

            {children}

        </UserContext.Provider>
    );
}