import React, { useState, useEffect } from "react";
import { View } from "react-native";
import PopupContext from "./index";

export default function ({ contextRef, children }) {
    const [component, setComponent] = useState(null);

    function showPopup(component, viewStyle, ...viewRest) {
        setComponent((
            <View style={{
                position: "absolute",
                left: 0,
                top: 0,
                right: 0,
                bottom: 0,
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "#000000CC",
                ...viewStyle
            }} {...viewRest}>

                {component}

            </View>
        ));
    }

    function isPopupVisible() {
        return component !== null;
    }

    function hidePopup() {
        setComponent(null);
    }

    const context = { isPopupVisible, showPopup, hidePopup };

    useEffect(() => {
        if (contextRef) {
            contextRef(context);
        }
    });

    return (
        <PopupContext.Provider value={context}>
            {children}
            {component}
        </PopupContext.Provider>
    );
}