/**
 * @author João Liliu
 * @description Popup context
 * @global
 * @typedef {Object} PopupContextValues
 * @property {Boolean} isPopupVisible
 * @property {Function} showPopup
 * @property {Function} hidePopup
 */

import { createContext, useContext } from "react";

const PopupContext = createContext({});

/**
 * @description Hook to use PopupContext
 * @returns {PopupContextValues} PopupContext
 */
export function usePopup(popupCtx) {
    return popupCtx || useContext(PopupContext);
}

export default PopupContext;