/**
 * @author João Liliu
 * @description App context
 * @global
 * @typedef {Object} AppContextValues
 * @property {Object} globalValues - Global app values
 * @property {Function} setGlobalValue - Set global app value
 * @property {Object} currentFile - Current file
 * @property {Object} appData - Current app config data
 */

import { createContext, useContext } from "react";

const AppContext = createContext({});

/**
 * @description Hook to use AppContext
 * @returns {AppContextValues} AppContext
 */
export function useAppContext() {
    return useContext(AppContext);
}

export default AppContext;