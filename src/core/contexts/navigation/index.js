/**
 * @author João Liliu
 * @description Navigation context
 * @global
 * @typedef {Object} NavigationContextValues
 * @property {Function} navigate - Navigate to screen
 * @property {Function} setLoading
 * @property {Function} getPageValue
 * @property {Function} setPageValue
 * @property {Function} setPageData
 * @property {Object} pageData
 * @property {Object} currentScreen
 * @property {Object} pageParams
 * @property {Object} pageContexts
 * @property {Function} addPageContext
 * @property {Function} removePageContext
 * @property {Function} showPopup
 * @property {Function} hidePopup
 * @property {Boolean} activeScroll
 * @property {Function} setActiveScroll
 * @property {NavigationContextPopup} popup
 * @property {NavigationContextActions} actions
 * @property {Function} subscribeValuesWatcher
 * @property {Function} unsubscribeValuesWatcher
 * 
 * @typedef {Object} NavigationContextPopup
 * @property {Function} show
 * @property {Function} hide
 * 
 * @typedef {Object} NavigationContextActions
 * @property {Function} execute
 * @property {Function} executeSync
 */

import { createContext, useContext } from "react";

const NavigationContext = createContext({});

/**
 * @description Hook to use NavigationContext
 * @returns {NavigationContextValues} NavigationContext
 */
export function useNavigationContext() {
    return useContext(NavigationContext);
}

export default NavigationContext;