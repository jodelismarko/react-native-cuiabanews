/**
 * @author João Liliu
 * @description AppComponent context.
 * 
 * @global
 * @typedef AppComponentContextValues
 * @property {Function} asyncSafeRun
 * @property {Function} forceUpdate
 * @property {Boolean} isMounted
 */

import { createContext, useContext } from "react";

const AppComponentContext = createContext({});

/**
 * @description Hook to use AppComponentContext
 * @returns {AppComponentContextValues}
 */
export function useComponentContext() {
    return useContext(AppComponentContext);
}

export default AppComponentContext;