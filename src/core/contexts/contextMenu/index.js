/**
 * @author João Liliu
 * @description Context Menu context
 * @global
 * @typedef {Object} ContextMenuContextValues
 * @property {Boolean} isMenuVisible
 * @property {Function} showContextMenu
 * @property {Function} hideContextMenu
 */

import { createContext, useContext } from "react";

const ContextMenuContext = createContext({});

/**
 * @description Hook to use ContextMenuContext
 * @returns {ContextMenuContextValues} ContextMenuContext
 */
export function useContextMenu(ctxMenu) {
    return ctxMenu || useContext(ContextMenuContext);
};

export default ContextMenuContext