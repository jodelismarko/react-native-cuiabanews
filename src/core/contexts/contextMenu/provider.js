import React, { useState, useEffect } from "react";
import { View, TouchableWithoutFeedback } from "react-native";
import ContextMenuContext from "./index";

export default function ({ contextRef, children }) {
    const [component, setComponent] = useState(null);

    function showContextMenu(component, { left, top, right, bottom }, viewStyle, ...viewRest) {

        const pos = { left, top, right, bottom };

        const view = (
            <TouchableWithoutFeedback onPress={hideContextMenu}>
                <View style={{
                    position: "absolute",
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: "transparent",
                }}>
                    <TouchableWithoutFeedback>
                        <View style={[viewStyle, pos, { position: "absolute" }]} {...viewRest}>
                            {component}
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </TouchableWithoutFeedback>
        );

        setComponent(view);
    }

    function isMenuVisible() {
        return component !== null;
    }

    function hideContextMenu() {
        setComponent(null);
    }

    const context = { isMenuVisible, showContextMenu, hideContextMenu };

    useEffect(() => {
        if (contextRef) {
            contextRef(context);
        }
    });

    return (
        <ContextMenuContext.Provider value={context}>
            {children}
            {component}
        </ContextMenuContext.Provider>
    );
}