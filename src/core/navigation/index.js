import React, { useState, useEffect, Suspense, useRef } from "react";
import { View } from "react-native";
import { Screens } from "../../mappings/components";
import Loader from "../../components/suspenseLoader";
import LoadingView from "../../components/loadingView";
import { JsUtils } from "../utils";
import AppComponent from "../components/appComponent";
import {
    NavigationContext,
    ContextMenuProvider,
    PopupProvider,
    useUserContext,
    useAppContext
} from "../contexts";
import { useActions, useEventsHandler } from "../helpers";

const EVENT_NAME = "_VALUES";

export default function ({ config }) {
    const [currentScreen, setCurrentScreen] = useState(null);
    const [top, setTop] = useState(false);
    const [activeScroll, setActiveScroll] = useState(true);
    const [loader, setLoader] = useState(null);
    const loaderStack = useRef([]);
    const contextMenuRef = useRef(null);
    const [pageData, setPageData] = useState({});
    const [pageParams, setPageParams] = useState(null);

    const pageContexts = useRef({});
    const popupRef = useRef(null);

    const { subscribe, unsubscribe, unsubscribeAll, fireEventCallbacks } = useEventsHandler();

    const userCtx = useUserContext();
    const appCtx = useAppContext();

    const _navigationCtxValues = {
        navigate,
        setLoading,
        activeScroll,
        setActiveScroll,
        getPageValue,
        setPageValue,
        setPageData,
        currentScreen,
        pageData,
        pageContexts,
        addPageContext,
        removePageContext,
        showPopup,
        hidePopup: () => popupRef.current.hidePopup(),
        popup: {
            show: (component, viewStyle, ...viewRest) => popupRef.current.showPopup(component, viewStyle, ...viewRest),
            hide: () => popupRef.current.hidePopup()
        }
    };

    const { execute, executeSync } = useActions({
        appCtx: appCtx,
        navigationCtx: _navigationCtxValues,
        userCtx: userCtx
    });

    function setLoading(loaderView, force) {
        if (force) {
            loaderStack.current = [];
            setLoader(loaderView);
            return;
        }

        if (loaderView && (typeof loaderView === "object" || loaderView === true)) {
            loaderStack.current.push(loaderView);
            setLoader(loaderView);
        } else {
            var nextLoader = null;

            if (loaderStack.current.length > 0)
                loaderStack.current.pop();

            if (loaderStack.current.length > 0) {
                nextLoader = loaderStack.current[loaderStack.current.length - 1];
            }
            setLoader(nextLoader);
        }
    }

    function addPageContext(name, context) {
        const tmp = {};
        tmp[name] = context;

        pageContexts.current = { ...pageContexts.current, ...tmp };
    }

    function removePageContext(name) {
        return delete pageContexts.current[name];
    }

    function parseScreenName(name) {
        name = name.replace("/pt-PT", "/").replace("//", "/");
        name = name.indexOf("?") > -1 ? name.substring(0, name.indexOf("?")) : name;
        return name;
    }

    function navigate(screen, params = {}, force) {
        screen = parseScreenName(screen);
        contextMenuRef.current.hideContextMenu();
        if (currentScreen && currentScreen.name === screen) return;

        let screenConfig = config.screens.find(s => s.name === screen);
        if (!screenConfig) throw `Screen '${screen}' is not configured in navigation.`;

        if (!force && currentScreen && currentScreen.onLeave) {
            execute(currentScreen.onLeave, {
                _screen: screen,
                _params: params
            }).then((res) => {
                if (res !== false) {
                    _navigate(screenConfig, params);
                }
            });
        } else {
            _navigate(screenConfig, params);
        }
    }

    function _navigate(screenConfig, params) {
        setLoading(true);

        unsubscribeAll();
        setPageData({});
        setTimeout(() => pageContexts.current = {});
        loadData(screenConfig.onLoad);

        setTimeout(() => {
            setPageParams(params);
            setCurrentScreen(screenConfig);
        });
    }

    function toTop(stauts) {
        setTop(stauts);
    }

    function toActiveScroll(stauts) {
        setActiveScroll(stauts);
    }

    function showPopup(id, args) {
        if (currentScreen) {
            if (!args) args = {};
            if (!args.backgroundColor) args.backgroundColor = "#000000CC";

            const modal = currentScreen.modals
                && currentScreen.modals.find(m => m.name === id);

            const modalConfig = {
                type: "FormModal",
                props: {
                    steps: modal.props.components
                },
                styleName: modal.styleName
            }

            if (modal) {
                popupRef.current.showPopup(
                    <AppComponent
                        config={modalConfig}
                        parentProps={{ initialStep: args.step }} />,
                    { backgroundColor: args.backgroundColor }
                );
            }
        }
    }

    function getScreen() {
        if (!currentScreen) return null;

        const Screen = Screens[currentScreen.type];

        return (
            <Suspense fallback={<Loader />}>
                <Screen
                    key={currentScreen.name}
                    navigation={{
                        navigate,
                        toTop,
                        top,
                        toActiveScroll,
                        activeScroll,
                        currentScreen,
                        setLoading
                    }}
                    props={currentScreen.props}
                    styleName={currentScreen.styleName} />
            </Suspense>
        );
    }

    function getHeader() {
        return getSectionContainer("header", config.headers);
    }

    function getSectionContainer(sectionName, componentGroup) {
        if (!currentScreen || !currentScreen[sectionName]) return null;

        const sectionConfig = componentGroup.find(h => h.name === currentScreen[sectionName]);

        return sectionConfig && (
            <AppComponent
                navigation={{
                    navigate,
                    toTop,
                    toActiveScroll,
                    activeScroll,
                    top,
                    currentScreen
                }}
                config={{
                    type: "Container",
                    ...sectionConfig
                }}
            />
        );
    }

    function loadData(loadActions) {
        if (!loadActions) return;

        execute(loadActions).then(data => {
            if (data) {
                setPageData(data);
            }
        });
    }

    function getPageValue(name) {
        return JsUtils.getDeepValue(pageData, name);
    }

    function setPageValue(name, value) {
        const newData = pageData;
        newData[name] = value;
        setPageData({ ...newData });
    }

    function subscribeValuesWatcher(action) {
        subscribe(EVENT_NAME, action);
    }

    function unsubscribeValuesWatcher(action) {
        unsubscribe(EVENT_NAME, action);
    }

    useEffect(() => {
        const initial = config.initialScreen || config.screens[0].name;
        navigate(initial);

        return () => {
            setPageData({});
            setPageParams({});
            setCurrentScreen(null);
        };
    }, []);

    useEffect(() => {
        fireEventCallbacks(EVENT_NAME);
    }, [pageData, pageContexts.current, userCtx.user, userCtx.authenticated]);

    return (
        <NavigationContext.Provider value={{
            ..._navigationCtxValues,
            actions: { execute, executeSync },
            subscribeValuesWatcher,
            unsubscribeValuesWatcher
        }}>
            <ContextMenuProvider contextRef={ref => contextMenuRef.current = ref}>
                <PopupProvider contextRef={ref => popupRef.current = ref}>
                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            {getScreen()}
                        </View>
                        {getHeader()}
                    </View>
                </PopupProvider>
            </ContextMenuProvider>
            {loader && <LoadingView {...(typeof loader === "object" ? loader : {})} />}
        </NavigationContext.Provider>
    );
}