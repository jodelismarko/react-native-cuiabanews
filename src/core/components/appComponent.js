/**
 * @author João Liliu
 * @description Component that renders dynamic content
 * 
 * @typedef {Object} AppComponentConfig
 * @property {string} type - Component type
 * @property {Object} props - Component properties
 * @property {string} styleName - Component style name
 *
 * @typedef {Object} AppComponentProps
 * @property {Object} navigation - [DEPRECATED] Use NavigationContext instead
 * @property {AppComponentConfig} config - Component configuration
 * @property {Object} parentProps - Parent properties
 */

import React, { Suspense, useEffect, useRef } from "react";
import { useForceUpdate } from "../helpers";
import { Components } from "../../mappings/components";
import Loader from "../../components/suspenseLoader";
import { AppComponentContext } from "../contexts";
import ErrorBoundary from "./errorBoundary";
import { Text } from "react-native";

const __DEBUG__  = false;

export function renderAppComponents({ navigation, components, parentProps }) {
    return components && components.map((config, index) => (
        <AppComponent
            key={index}
            config={config}
            navigation={navigation}
            parentProps={parentProps} />
    ));
}

/**
 * @param {AppComponentProps} args
 */
function AppComponentBase(args) {
    const { navigation, config: { type, props, styleName }, parentProps } = args;
    const isMounted = useRef(true);

    const Tag = Components[type];

    const [componentKey, forceUpdate] = useForceUpdate();

    /**
     * @method asyncSafeRun
     * @description Run a function in safe mode (handle unmounted component)
     * @param {Function} action
     * @returns {Promise} - Result of the action execution
     * @throws "error.unmounted = true" if the component is unmounted
     */
    async function asyncSafeRun(action) {
        if (typeof action !== "function") return;

        const result = action();
        if (!(result instanceof Promise)) {
            return result;
        }

        const promiseResult = await result;

        if (!isMounted.current) {
            return;
        }

        return promiseResult;
    }

    useEffect(() => {
        isMounted.current = true;

        return () => {
            isMounted.current = false;
        };
    }, []);

    return Tag
        ? (
            <AppComponentContext.Provider value={{
                asyncSafeRun,
                forceUpdate,
                isMounted: isMounted.current
            }}>
                <Tag
                    key={componentKey}
                    navigation={navigation}
                    props={props}
                    styleName={styleName}
                    parentProps={parentProps} />
            </AppComponentContext.Provider>
        )
        : __DEBUG__
            ? <Text key={key} style={{ color: "red" }}>Component {type} is not mapped.</Text>
            : null;
}

/**
 * @param {AppComponentProps} props
 */
export default function AppComponent(props) {
    return (
        <Suspense fallback={<Loader />}>
            <ErrorBoundary details={props.config}>
                <AppComponentBase {...props} />
            </ErrorBoundary>
        </Suspense>
    )
}
