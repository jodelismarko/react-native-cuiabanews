/**
 * @typedef {Object} ErrorBoundaryProps
 * @property {Object} details - Error details
 * @property {function} onError - Function to call on error
 */

import React from "react";
import { Text } from "react-native";

const __DEBUG__  = false;

export default class ErrorBoundary extends React.Component {
    /**
     * @param {ErrorBoundaryProps} props
     */
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    componentDidCatch(error, info) {
        this.setState({ hasError: true });

        if (this.props.onError)
            this.props.onError(error, info, this.props.details);
        else
            console.log("COMPONENT ERROR", error, info, this.props.details);
    }

    render() {
        if (!this.state.hasError)
            return this.props.children;

        if (__DEBUG__)
            return <Text style={{ color: "red" }}>Component Error.</Text>;

        return null;
    }
}