/**
 * @author João Liliu
 * @description Events
 */

import { useEffect, useRef } from "react";

/**
 * @description Hook to use events handler.
 */
export function useEventsHandler() {
    const events = useRef({});

    useEffect(() => {
        return () => events.current = {};
    }, []);

    /**
     * Subscribe an event.
     * @param {String} eventName - Name of the event
     * @param {Function} callback - Callback
     */
    function subscribe(eventName, callback) {
        if (!eventName) return;

        if (!events.current[eventName])
            events.current[eventName] = [];

        events.current[eventName].push(callback);
    }

    /**
     * Unsubscribe an event.
     * @param {String} eventName - Name of the event
     * @param {Function} callback - Callback
     */
    function unsubscribe(eventName, callback) {
        if (events.current[eventName]) {
            const index = events.current[eventName].indexOf(callback);
            if (index !== -1) {
                events.current[eventName].splice(index, 1);
            }
        }
    }

    /**
     * 
     * @param {String} eventName - Name of the event.
     * If null all events will be unsubscribed.
     */
    function unsubscribeAll(eventName = null) {
        if (!eventName)
            events.current = {};
        else if (events.current[eventName])
            events.current[eventName] = [];
    }

    /**
     * Fire all subscribed callbacks to specified event.
     * @param {*} eventName 
     * @param  {...any} args 
     */
    function fireEventCallbacks(eventName, ...args) {
        if (events.current[eventName]) {
            events.current[eventName].forEach(callback => callback(...args));
        }
    }

    return { subscribe, unsubscribe, unsubscribeAll, fireEventCallbacks };
}