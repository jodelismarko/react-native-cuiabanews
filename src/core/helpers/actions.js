/**
 * @author João Liliu
 * @description Actions
 * 
 * @typedef {Object} ActionsProps
 * @property {Object} navigationCtx
 * @property {Object} userCtx
 * @property {Object} appCtx
 */

import axios from "axios";
import { JsUtils } from "../utils";
import SyntaxParser from "./syntax";

/**
 * @description Hook to use external actions executor handler
 * @param {ActionsProps} props 
 */
export function useActions({ appCtx, userCtx, navigationCtx }) {

    const DefaultActions = {
        navigate: navigationCtx.navigate,
        is: (value, comparison) => value == comparison,
        not: (value, comparison) => value != comparison,
        in: (value, comparison) => comparison instanceof Array && comparison.includes(value),
        notin: (value, comparison) => comparison instanceof Array && !(comparison.includes(value)),
        concat: (...values) => values.join(""),
        convertToDate: JsUtils.stringToDate,
        dateToString: JsUtils.dateToString,
        parseStringDate: JsUtils.parseStringDate,
        formatMoney: JsUtils.formatMoneyString,
        parseValue: (value, values, defaultValue = null) => values[value] || defaultValue,
        axios,
        data: {
            currentFile: appCtx.currentFile
        },
        user: {
            getValue: userCtx.getUserValue,
            signIn: userCtx.signIn,
            signOut: userCtx.signOut,
            updateUser: userCtx.updateUser,
            authenticated: userCtx.authenticated,
            data: userCtx.user
        },
        page: {
            currentScreen: navigationCtx.currentScreen,
            navigate: navigationCtx.navigate,
            showPopup: navigationCtx.showPopup,
            hidePopup: navigationCtx.hidePopup,
            getValue: navigationCtx.getPageValue, // delete
            getPageValue: navigationCtx.getPageValue,
            setPageValue: navigationCtx.setPageValue,
            setData: navigationCtx.setPageData,
            data: navigationCtx.pageData,
            setLoading: navigationCtx.setLoading
        }
    };

    /**
     * @description Make an API request with loading view
     * @param {Object} api 
     * @param {String} method 
     * @param {Function} success 
     * @param {Function} fail 
     * @param  {...any} args 
     */
    function apiRequest(api, method, success, fail, ...args) {
        navigationCtx.setLoading({ viewStyle: { backgroundColor: "#FFFFFFCC" } });
        var result = apiRequestNoLoading(api, method, success, fail, ...args).finally(() => navigationCtx.setLoading(null));
        return result;
    }

    /**
     * @description Make an API request
     * @param {Object} api 
     * @param {String} method 
     * @param {Function} success 
     * @param {Function} fail 
     * @param  {...any} args 
     */
    function apiRequestNoLoading(api, method, success, fail, ...args) {
        return api[method](...args)
            .then(response => {
                if (typeof success === "function")
                    success(response);
            }).catch(err => {
                if (typeof fail === "function")
                    fail(err);

                handleError(err, { API: api, ACTION: method }, "Service request error");
            });
    }

    /**
     * @description Executes an asynchronous external action
     * @param {String} instructionsString 
     * @param {Object} contextValues
     */
    async function execute(instructionsString, contextValues = {}) {
        let strFunction;
        if (!instructionsString || !(strFunction = instructionsString.trim())) return;


        const ctx = contextValues
            ? { ...DefaultActions, ...navigationCtx.pageContexts.current, ...contextValues }
            : DefaultActions;

        //TODO: Make root contextActions obsolete
        if (contextValues) {
            ctx.caller = contextValues;
        }

        if (strFunction.startsWith("function")) {
            try {
                const f = eval(`(() => ${strFunction})()`);
                let result = f(ctx);
                if (result instanceof Promise)
                    result = await result;

                return result;
            } catch (err) {
                handleError(err, strFunction);
                return;
            }
        } else { //Obsolete
            const configs = SyntaxParser.parseActions(strFunction, ctx);

            for (const config of configs) {
                const action = JsUtils.getDeepValue(ctx, config.name);

                if (typeof action !== "function") {
                    handleError(`Invalid action "${config.name}"`, strFunction);
                    return;
                }

                const result = action(...config.params);
                if (result instanceof Promise)
                    await result;
            }
        }
    }

    /**
     * @description Executes a synchronous external action
     * @param {String} instructionsString 
     * @param {Object} contextValues 
     */
    function executeSync(instructionsString, contextValues = {}) {
        let strFunction;
        if (!instructionsString || !(strFunction = instructionsString.trim())) return;

        const ctx = contextValues
            ? { ...DefaultActions, ...navigationCtx.pageContexts.current, ...contextValues }
            : DefaultActions;

        //TODO: Make root contextActions obsolete
        if (contextValues) {
            ctx.caller = contextValues;
        }

        try {
            const f = eval(`(() => ${strFunction})()`);
            return f(ctx);
        } catch (err) {
            handleError(err, strFunction);
        }
    }

    /**
     * 
     * @param {*} error
     * @param {*} extraInfo 
     * @param {String} prefix 
     */
    function handleError(error, extraInfo, prefix = "Error executing action.") {
        console.log(prefix, extraInfo, error);

        let errorStr = error;
        if (typeof error !== "string") {
            try {
                errorStr = JSON.stringify(error);
            } catch {
                errorStr = "UNSPECIFIED!";
            }
        }

        let extraInfoStr = extraInfo;
        if (typeof extraInfo !== "string") {
            try {
                extraInfoStr = JSON.stringify(extraInfo);
            } catch {
                extraInfoStr = "UNSPECIFIED!";
            }
        }

        
    }

    return { execute, executeSync };
}
