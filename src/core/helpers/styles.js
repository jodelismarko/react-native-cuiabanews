/**
 * Get an array of styles by names
 * @param {Object} styleGroup
 * @param {String} styleName 
 * @param {String} suffix
 * @returns {Array} Array of styles
 */
export function getAllStyles(styleGroup, styleName, suffix) {
    if (!styleName)
        return null;

    const styleNames = styleName.split(",");
    var stylesArray = styleNames.map(x => styleGroup[
        suffix
            ? (x.trim() + suffix)
            : x.trim()
    ]);

    return stylesArray;
}
