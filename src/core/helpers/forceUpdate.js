import { useEffect, useRef, useState } from "react";

export function useForceUpdate() {
    const isMounted = useRef(true);
    const [updateKey, setUpdateKey] = useState(Math.random());

    useEffect(() => {
        isMounted.current = true;

        return () => {
            isMounted.current = false;
        }
    }, []);

    /**
     * @description Forces the component to rerender
     */
    function forceUpdate() {
        setTimeout(() => {
            if (isMounted.current)
                setUpdateKey(Math.random());
        });
    }

    return [updateKey, forceUpdate];
}