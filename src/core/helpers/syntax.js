/**
 * @deprecated Since support to JavaScript functions
 */
export class ActionConfig {
    constructor(name, context) {
        this.name = name.trim();
        this.params = [];
        this.context = context;
        this.callbackJoin = false;
        this.nonEvalIndexes = [];
    }

    addParam(param) {
        if (typeof (param) === "string") {
            param = param.trim();

            if (param.startsWith("&")) {
                param = param.substring(1);
                this.nonEvalIndexes.push(this.params.length);
            } else {
                param = eval(param);
            }

            this.params.push(param);
        } else if (param instanceof ActionConfig) {
            if (this.callbackJoin && typeof (this.params[this.params.length - 1]) === "function") {
                let callback = ((previous) => this.createFunction(param, previous))(this.params.pop());

                this.params.push(callback);
            } else {
                this.params.push(this.createFunction(param));
            }
        }
    }

    createFunction(config, previousFunction) {
        if (config.name.startsWith("&")) {
            const immediateAction = JsUtils.getDeepValue(config.context, config.name.substring(1));
            return immediateAction(...config.params);
        }

        return (values, ...rest) => {
            if (previousFunction) {
                previousFunction(values, ...rest);
            }

            for (const index of config.nonEvalIndexes) {
                let val = values;

                if (typeof values === "object") {
                    const name = config.params[index];
                    val = JsUtils.getDeepValue(values, name);
                }

                config.params[index] = val;
            }

            const action = JsUtils.getDeepValue(config.context, config.name);
            if (typeof action === "function") {
                action(...config.params, ...rest);
            } else {
                action[config.name](...config.params, ...rest);
            }
        }
    }

    execute() {
        this.context[this.name](...this.params);
    }
}

/**
 * @deprecated Since support to JavaScript functions
 */
const SyntaxParser = {
    /**
     * @deprecated Since support to JavaScript functions
     * @param {String} code 
     */
    tokenizeActionsConfig(code) {
        var code = code.split(/\\./).join(''),
            regex = /\(|\)|,|\[|\]|\r|\n+/mg,
            tokens = [],
            pos = 0;

        for (var matches; matches = regex.exec(code); pos = regex.lastIndex) {
            var match = matches[0],
                matchStart = regex.lastIndex - match.length;

            if (pos < matchStart)
                tokens.push(code.substring(pos, matchStart));

            tokens.push(match);
        }

        if (pos < code.length)
            tokens.push(code.substring(pos));

        return tokens;
    },

    /**
     * @deprecated Since support to JavaScript functions
     * @param {String} instructionsString 
     * @param {Object} context 
     */
    parseActions(instructionsString, context) {
        const configs = [];
        const tokens = this.tokenizeActionsConfig(instructionsString);

        let level = -1;
        let parentActions = [];
        let currentAction = null;
        let inString = false;

        for (let index = 1; index < tokens.length; index++) {
            const token = tokens[index];
            const previousToken = tokens[index - 1];

            try {
                switch (token) {
                    case "(":
                        if (currentAction) {
                            parentActions.push(currentAction);
                        }

                        currentAction = new ActionConfig(previousToken, context);
                        level++;
                        break;

                    case ")":
                        if (previousToken !== "(" && previousToken !== ")") {
                            currentAction.addParam(previousToken);
                        }

                        const lastAction = parentActions.pop();

                        if (lastAction) {
                            lastAction.addParam(currentAction);
                        } else {
                            configs.push(currentAction);
                        }

                        currentAction = lastAction;
                        level--;
                        break;

                    case "[":
                        if (currentAction) {
                            currentAction.callbackJoin = true;
                        }
                        break;

                    case "]":
                        if (currentAction) {
                            currentAction.callbackJoin = false;
                        }
                        break;

                    case ",":
                        if (![")", "]", "'", " "].includes(previousToken)) {
                            currentAction.addParam(previousToken);
                        }
                        break;
                }
            } catch (err) {
                throw `Parser error on index ${index}, token '${token}', previous token '${previousToken}'.\r\n` + err;
            }
        }

        return configs;
    }
};

export default SyntaxParser;