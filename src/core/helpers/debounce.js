import { useRef } from "react";

export function useDebouncer(defaultDelay = 200) {
    const timeoutId = useRef(null);

    function debounce(action, delay) {
        cancelDebounce();
        timeoutId.current = setTimeout(action, delay || defaultDelay);
    }

    function cancelDebounce() {
        clearInterval(timeoutId.current);
    }

    return { debounce, cancelDebounce };
}