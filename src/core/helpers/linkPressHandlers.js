import { useWebView } from "../../components/webView";
import { useNavigationContext } from "../contexts";
import { useFormGroupContext } from "../../helpers/forms";

export function useOnLinkPressHandler({ linkType, href, getParams }) {
    const { opeWebViewPopup } = useWebView();
    const { navigate, actions: { execute } } = useNavigationContext();
    const formContext = useFormGroupContext();

    return {
        onLinkPress: () => {

            switch (linkType) {
                case "internal":
                    if (getParams) {
                        execute(getParams).then(params => {
                            navigate(href, params);
                        });
                    } else {
                        navigate(href);
                    }
                    break;

                case "external":
                    opeWebViewPopup({ uri: href });
                    break;

                case "pdf":
                    opeWebViewPopup({ uri: href, pdf: true });
                    break;

                case "action":
                    if (formContext && formContext.executeActions)
                        formContext.executeActions(href);
                    else
                        execute(href);
                    break;

                default:
                    console.log(`Link Type "${linkType}" is not recognized.`);
                    break;
            }
        }
    }
};

export function useOnHtmlLinkPressHandler() {
    const { opeWebViewPopup } = useWebView();
    const { navigate } = useNavigationContext();

    return {
        onLinkPress: (href) => {
            if (!href)
                return;

            if (href.startsWith("/-/media")) {
                opeWebViewPopup({ uri: href, pdf: true })
            }
            else if (href.startsWith("/")) {
                navigate(href);
            } else {
                opeWebViewPopup({ uri: href, pdf: href.endsWith(".pdf") })
            }

        }
    }
};