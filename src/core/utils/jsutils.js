export const JsUtils = {
    sleep: (time) => new Promise(resolve => setTimeout(resolve, time)),

    getStringHashCode(obj) {
        if (typeof obj !== "string") {
            obj = JSON.stringify(obj);
        }

        let hash = 0;
        let char;

        for (let i = 0; i < obj.length; i++) {
            char = obj.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash |= 0;
        }

        return hash.toString();
    },

    pascalize(str) {
        return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
            if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
            return match.toUpperCase();
        });
    },

    uniqueId: () => Math.random().toString(36).substr(2, 9),

    dateToString(date, includeTime) {
        if (!(date instanceof Date)) return null;

        let result = `${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)}`;
        return includeTime
            ? `${result} ${("0" + (date.getHours())).slice(-2)} ${("0" + (date.getMinutes())).slice(-2)} ${("0" + (date.getSeconds())).slice(-2)}`
            : result;
    },

    stringToDate(str, format = "yyyy-MM-dd") {
        if (!str) return null;

        if (str instanceof Date) return str;

        const regex = /\-|\/|\ |\./;

        const formatItems = format.split(regex);
        const dateItems = str.split(regex);
        const monthIndex = formatItems.indexOf("MM");
        const dayIndex = formatItems.indexOf("dd");
        const yearIndex = formatItems.indexOf("yyyy");
        let month = parseInt(dateItems[monthIndex]);
        month -= 1;

        const formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
        return formatedDate;
    },

    parseStringDate(str, fromFormat, toFormat) {
        if (!str) return null;

        const parts = jsutils.getStringDateParts(str, fromFormat);

        const regex = /\-|\/|\ |\./;

        const toFormatItems = toFormat.split(regex);
        const monthIndex = toFormatItems.indexOf("MM");
        const dayIndex = toFormatItems.indexOf("dd");
        let yearIndex = toFormatItems.indexOf("yyyy");
        
        if(yearIndex < 0)
            yearIndex = toFormatItems.indexOf("yy");

        const result = new Array(3);
        result[yearIndex] = ("0" + parts[0]).slice(-toFormatItems[yearIndex].length);;
        result[monthIndex] = ("0" + parts[1]).slice(-2);
        result[dayIndex] = ("0" + parts[2]).slice(-2);

        const separator = toFormat.replace(/MM|dd|y/g, "")[0];
        return result.join(separator);
    },

    getStringDateParts(str, format) {
        const regex = /\-|\/|\ |\./;
        const formatItems = format.split(regex);
        const dateItems = str.split(regex);

        const monthIndex = formatItems.indexOf("MM");
        const dayIndex = formatItems.indexOf("dd");
        const yearIndex = formatItems.indexOf("yyyy");
        let month = parseInt(dateItems[monthIndex]);

        return [
            parseInt(dateItems[yearIndex]),
            month,
            parseInt(dateItems[dayIndex])
        ];
    },

    formatMoneyString(value, editMode = false, decimalPlaces = 2, decimalSeparator = ",", symbol = "€") {
        if (!value && value !== 0) return null;

        value = Number(value.toString().replace(new RegExp(` |${symbol || ""}`, "gm"), "").replace(",", "."));

        if (typeof value === "number") {
            if (!editMode) {
                value = value.toFixed(decimalPlaces);
            }

            value = value.toString().replace(".", decimalSeparator);

            if (!editMode && symbol) {
                value += symbol;
            }
        }

        return value;
    },

    formatMoneyNumber(value, symbol = "€") {
        if (typeof value === "number")
            return value;
        else if (typeof value === "string") {
            const num = Number(value.replace(new RegExp(` |${symbol}`, "gm"), "").replace(",", "."));

            if (num || num === 0) return num;
        }

        return null;
    },

    formatNumber(value) {
        if (typeof value === "number")
            return value;
        else if (typeof value === "string") {
            return value
                ? Number(value.toString().replace(",", "."))
                : null;
        }

        return null;
    },

    assign(obj, keyPath, value) {
        if (typeof keyPath === "string") keyPath = keyPath.split(".");

        lastKeyIndex = keyPath.length - 1;
        for (var i = 0; i < lastKeyIndex; ++i) {
            key = keyPath[i];
            if (!(key in obj)) {
                obj[key] = {}
            }
            obj = obj[key];
        }
        obj[keyPath[lastKeyIndex]] = value;
    },

    toBoolean(value) {
        value = (value || "").toString().toLowerCase();
        return ["true", "1"].includes(value);
    },

    groupBy(array, key) {
        return array.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    },

    mergeArrayOfObjects(oldArray, newArray, keyName) {
        for (var entry of newArray) {
            var oldEntryIndex = oldArray.findIndex(x => x.name == entry[keyName]);
            if (oldEntryIndex > -1) {
                oldArray[oldEntryIndex] = entry;
            } else {
                oldArray.push(entry);
            }
        }
    },

    formatString(string, ...args) {
        return string.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match;
        });
    },

    getDeepValue(obj, path) {
        if (!obj || !path) return null;

        const splittedPath = path.split(".");

        let currentValue = typeof obj === "function" ? obj() : obj;
        for (const part of splittedPath) {
            if (currentValue == null) {
                return null;
            }

            if (typeof currentValue === "function") {
                currentValue = currentValue(part)
            } else if (!currentValue.hasOwnProperty(part)) {
                return null;
            } else {
                currentValue = currentValue[part];
            }
        }

        return currentValue;
    },

    parseSafeInteger(value) {
        if ((!value && value !== 0) || isNaN(value)) return null;

        value = Number(value);

        if (Number.isSafeInteger(value))
            return value;
        else
            return value < 0 ? Number.MIN_SAFE_INTEGER : Number.MAX_SAFE_INTEGER;
    },

    tokensReplacer(content, tokens) {
        if (!content || !tokens || !tokens)
            return content;

        Object.keys(tokens).forEach(element => {
            content = content.replace(`[${element}]`, tokens[element])
        });

        return content;
    }
}
