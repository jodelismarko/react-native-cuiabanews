import axios from "axios";
import { JsUtils } from "../utils";
import AsyncStorage from "@react-native-community/async-storage";
import { Buffer } from "buffer";
import { DefaultKeyLocker } from "../helpers";

const Config = {
    mediaPath:''
}

class MediaStorage {
    constructor(basePath) {
        this.basePath = basePath;
        this.locked = {};
    }

    async getFile(url) {
        if (!url) {
            console.log("MediaStorage.GetFile - Url is empty");
            return null;
        }

        const key = JsUtils.getStringHashCode(url);

        return await DefaultKeyLocker.executeLocked(key, async () => {
            try {
                let b64File = await AsyncStorage.getItem(key);

                if (!b64File) {
                    const path = url.startsWith("/") ? url : url;
                    const { data, headers } = await axios.get(path, {
                        responseType: "arraybuffer",
                        timeout: 30000
                    });

                    b64File = `data:${headers["content-type"]};base64,${new Buffer(data, "binary").toString("base64")}`;

                    await this.saveFile(key, b64File);
                }

                return b64File;
            } catch (error) {
                // console.log(`Getting image ${url} error`, error);
                return null;
            }
        });
    }

    async saveFile(key, data) {
        try {
            await AsyncStorage.setItem(key, data);
        } catch (error) {
            // console.log(error);
        }
    }

    removeFile(name) {

    }

    clear() {

    }
}

export default new MediaStorage(Config.mediaPath);
